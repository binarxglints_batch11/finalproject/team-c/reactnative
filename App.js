import 'react-native-gesture-handler';
import React from 'react';
import RootStackScreen from './js/screen/RootStackScreen';
import {NavigationContainer} from '@react-navigation/native';
import {Provider, useDispatch} from 'react-redux';
import store from './js/Redux/store/index';

const App = ({navigation}) => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootStackScreen />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
