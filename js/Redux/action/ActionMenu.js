import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {useSelector} from 'react-redux';

export const GetMenu = (idServices, nameService) => async dispatch => {
  const token = await AsyncStorage.getItem('token');

  const data = await axios({
    method: 'GET',
    url: 'https://washme.gabatch11.my.id/search?service=' + idServices,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(res => {
      return res.data.data.data;
    })
    .catch(err => {
      console.log('err', err);
    });

  return dispatch({type: 'GET_SERVICES', payload: {data, nameService}});
};

export const GetDetailLaundry = idLaundry => async dispatch => {
  const token = await AsyncStorage.getItem('token');

  return axios({
    method: 'GET',
    url:
      'https://washme.gabatch11.my.id/details/' +
      idLaundry +
      '?service=1,2,3,4,5,6',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(res => {
      console.log('ini responnya', res.data.data);
      dispatch({
        type: 'GET_DETAIL_LAUNDRY',
        payload: res.data.data,
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const GetIdCustomer = id => {
  return {
    type: 'GET_ID_CUSTOMER',
    payload: id,
  };
};

export const GetProfile = () => async dispatch => {
  const token = await AsyncStorage.getItem('token');
  const userId = await AsyncStorage.getItem('id');
  // console.log(userId);

  var config = {
    method: 'get',
    url: 'https://washme.gabatch11.my.id/customer/' + userId,
    headers: {
      Accept: 'application/json',
      Authorization: 'bearer ' + token,
    },
  };

  axios(config)
    .then(function (res) {
      console.log('ini res coba', res);
      dispatch({
        type: 'GET_PROFILE',
        payload: res.data.data,
      });
    })
    .catch(function (error) {
      console.log('ini error', error.message);
    });
};

export const GetSeacrh = (
  servicesSelected,
  typesSelected,
) => async dispatch => {
  console.log('ini search services dan type', servicesSelected, typesSelected);
  const token = await AsyncStorage.getItem('token');

  const data = await axios({
    method: 'GET',
    url:
      'https://washme.gabatch11.my.id/search?service=' +
      servicesSelected +
      '&type=' +
      typesSelected,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(res => {
      console.log('ini responnya search', res.data.data.data);
      dispatch({
        type: 'GET_SEARCH',
        payload: res.data.data.data,
      });
    })
    .catch(err => {
      console.log('ini err', err.message);
    });
};

export const GetEditProfile = (name, mobile_phone) => async dispatch => {
  const token = await AsyncStorage.getItem('token');
  const userId = await AsyncStorage.getItem('id');

  var FormData = require('form-data');
  var data = new FormData();
  data.append('name', name);
  data.append('mobile_phone', mobile_phone);

  var config = {
    method: 'PUT',
    url: 'https://washme.gabatch11.my.id/customer/' + userId,
    headers: {
      Accept: 'application/json',
      Authorization: 'bearer ' + token,
    },
    data,
  };

  axios(config)
    .then(function (res) {
      console.log('ini res editProfile', res);
    })
    .catch(function (error) {
      console.log('ini error', error.message);
    });
};

export const PostCreateOrder = PaymentDetail => async dispatch => {
  const token = await AsyncStorage.getItem('token');
  console.log('ini data', PaymentDetail);

  var FormData = require('form-data');
  var formdata = new FormData();
  formdata.append('laundry_id', PaymentDetail.laundry_id);
  formdata.append('ordertype_id', PaymentDetail.ordertype_id);
  formdata.append('servicelist_id', PaymentDetail.servicelist_id);
  formdata.append('servicelevel_id', PaymentDetail.servicelevel_id);
  formdata.append('weight', PaymentDetail.weight);
  formdata.append('item', PaymentDetail.item);
  formdata.append('estimatePrice', PaymentDetail.estimatePrice);
  formdata.append('pickUpChoice', PaymentDetail.pickUpChoice);
  formdata.append('pickUpAddress', PaymentDetail.pickUpAddress);
  formdata.append('deliveryAddress', PaymentDetail.deliveryAddress);
  formdata.append('delivery_fee', PaymentDetail.delivery_fee);
  formdata.append('admin_charge', PaymentDetail.admin_charge);
  formdata.append('totalPrice', PaymentDetail.totalPrice);
  formdata.append('paymentType', PaymentDetail.paymentType);

  fetch('https://washme.gabatch11.my.id/order/create/', {
    method: 'POST',
    headers: {
      // 'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${token}`,
    },
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      console.log('Success:', data);
    })
    .catch(error => {
      console.error('Error:', error);
    });
};

export const GetOnProcess = () => async dispatch => {
  const token = await AsyncStorage.getItem('token');

  const data = await axios({
    method: 'GET',
    url: 'https://washme.gabatch11.my.id/order/status/',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(res => {
      console.log('ini responnya search', res.data.data);
      dispatch({
        type: 'GET_ON_PROCESS',
        payload: res.data.data,
      });
    })
    .catch(err => {
      console.log('ini err', err.message);
    });
};

export const GetOrderDetail = order_id => async dispatch => {
  console.log('id order', order_id);
  const token = await AsyncStorage.getItem('token');

  var config = {
    method: 'get',
    url: 'https://washme.gabatch11.my.id/order/' + order_id,
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };

  axios(config)
    .then(function (res) {
      console.log('respon order', res.data.data);
      dispatch({
        type: 'GET_ORDER_DETAIL',
        payload: res.data.data,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const GetFinish = () => async dispatch => {
  const token = await AsyncStorage.getItem('token');

  const data = await axios({
    method: 'GET',
    url: 'https://washme.gabatch11.my.id/order/statusfinish/',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then(res => {
      console.log('ini finish', res.data.data);
      dispatch({
        type: 'GET_FINISH',
        payload: res.data.data,
      });
    })
    .catch(err => {
      console.log('ini err', err.message);
    });
};
