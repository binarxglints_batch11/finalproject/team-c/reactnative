import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const getNearYou = () => {
  return dispatch => {
    AsyncStorage.getItem('token', (_error, userToken) => {
      console.log('token', userToken);
      axios({
        method: 'get',
        url: 'https://washme.gabatch11.my.id/laundry?page=1&limit=5',
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${userToken}`,
        },
      })
        .then(({data}) => {
          console.log('ini respon', data.data);
          dispatch({
            type: 'GET_ALL_LAUNDRY',
            payload: data.data.data,
          });
        })
        .catch(err => console.log('ini error', err));
    });
  };
};
