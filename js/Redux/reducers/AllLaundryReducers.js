const initialState = {
  AllLaundry: [],
  data: {},
  detail: {},
  idCustomer: '',
  profile: {},
  search: [],
  on_process: [],
  orderDetail: {},
  finish: [],
  // createOrder: [],
};

const AllLaundryReducers = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case 'GET_ALL_LAUNDRY':
      return {
        ...state,
        AllLaundry: payload,
      };
    case 'GET_SERVICES':
      return {
        ...state,
        data: payload,
      };
    case 'GET_DETAIL_LAUNDRY':
      return {
        ...state,
        detail: payload,
      };
    case 'GET_ID_CUSTOMER':
      return {
        ...state,
        idCustomer: payload,
      };
    case 'GET_PROFILE':
      return {
        ...state,
        profile: payload,
      };
    case 'GET_SEARCH':
      return {
        ...state,
        search: payload,
      };
    case 'GET_ON_PROCESS':
      return {
        ...state,
        on_process: payload,
      };
    case 'GET_ORDER_DETAIL':
      return {
        ...state,
        orderDetail: payload,
      };
    case 'GET_FINISH':
      return {
        ...state,
        finish: payload,
      };
    // case 'POST_CREATE_ORDER':
    //   return {
    //     ...state,
    //     createOrder: payload,
    //   };
    default:
      return state;
  }
};

export default AllLaundryReducers;
