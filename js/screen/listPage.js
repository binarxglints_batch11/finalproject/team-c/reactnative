import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  FlatList,
  TouchableOpacity,
  TextInput,
  Text,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SearchBar from '../screen/Component/Search/index';
import BGHome from '../assets/BGHome.png';
import LaundryCard from './Component/LaundryCard/index';
import {useDispatch, useSelector} from 'react-redux';
import {PostAddToList} from '../Redux/action/ActionMenu';

const listPage = props => {
  const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(PostAddToList());
  // }, []);

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGHome} />
      {/* header */}
      <View style={styles.header}>
        {/* header */}
        <View style={styles.header}>
          <TouchableOpacity
            style={{width: '90%'}}
            onPress={() => navigation.navigate('SearchPage')}>
            <SearchBar />
          </TouchableOpacity>
          <View style={{marginLeft: 10}}>
            <TouchableOpacity>
              <AntDesign name="bells" size={25} color={'white'} />
            </TouchableOpacity>
          </View>
        </View>
        {/* body */}
        {/* 
        <FlatList
          data={datalaundry}
          renderItem={props => {
            console.log('ini props', props.item);
            return (
              <View style={{marginBottom: 10}}>
                <LaundryCard
                  laundryname={props.item.laundryname}
                  price={props.item.price}
                  services={props.item.services}
                  weight={props.item.weight}
                />
              </View>
            );
          }}
        /> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  search: {
    width: '80%',
    height: 36,
    marginTop: 7,
    backgroundColor: 'white',
    color: 'black',
    borderRadius: 10,
    paddingHorizontal: 35,
    position: 'absolute',
  },
  header: {
    height: 50,
    marginTop: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  logo: {
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    height: 266,
    position: 'absolute',
    flex: 1,
  },
  logoNotif: {
    width: '20%',
    flexDirection: 'row',
    height: 36,
    position: 'absolute',
    flex: 1,
  },
});

export default listPage;
