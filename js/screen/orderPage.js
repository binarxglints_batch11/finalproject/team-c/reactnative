import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
  TextInput,
  ActivityIndicator,
  Text,
  StyleSheet,
  ImageBackground,
  ScrollView,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import CartOrder from '../assets/CartOrder.png';
import BGHome from '../assets/BGHome.png';
import SearchBar from '../screen/Component/Search/index';
import OrderCard from '../screen/Component/OrderCard/index';
import {useDispatch, useSelector} from 'react-redux';
import {
  GetFinish,
  GetOnProcess,
  GetOrderDetail,
} from '../Redux/action/ActionMenu';

const orderPage = props => {
  const {on_process, finish} = useSelector(state => state.AllLaundryReducers);
  console.log('status finish', finish);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(GetOnProcess());
    dispatch(GetFinish());
  }, []);

  const [onProcess, setOnProcess] = useState();

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGHome} />
      {/* header */}
      <View style={styles.header}>
        <TouchableOpacity
          style={{width: '90%'}}
          onPress={() => props.navigation.navigate('SearchPage')}>
          <SearchBar />
        </TouchableOpacity>
      </View>

      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <TouchableOpacity onPress={() => setOnProcess('on_process')}>
          <View style={{alignItems: 'center'}}>
            <Text
              style={
                onProcess == 'on_process' ? styles.Process2 : styles.Process
              }>
              On Process
            </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => setOnProcess('finished')}>
          <View style={{alignItems: 'center'}}>
            <Text
              style={
                onProcess == 'finished' ? styles.Process2 : styles.Process
              }>
              Finished
            </Text>
          </View>
        </TouchableOpacity>
      </View>

      <FlatList
        data={onProcess == 'finished' ? finish : on_process}
        keyExtractor={item => item.id}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              style={{alignItems: 'center', marginBottom: 10}}
              onPress={async () => {
                await dispatch(GetOrderDetail(item.id));
                props.navigation.navigate('OrderDetailPage');
              }}>
              <View
                style={{
                  backgroundColor: 'white',
                  width: '90%',
                  height: 200,

                  marginHorizontal: 10,
                  borderRadius: 10,
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 1,
                    height: 1,
                  },
                  shadowOpacity: 0.2,
                  shadowRadius: 1.41,

                  elevation: 2,
                  borderBottomLeftRadius: 10,
                  borderBottomRightRadius: 10,
                }}>
                <View
                  style={{
                    marginTop: 10,
                    marginBottom: 10,
                    marginHorizontal: 15,
                    flexDirection: 'column',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      marginTop: 10,
                      marginBottom: 5,
                    }}>
                    {item.laundry.name}
                  </Text>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      marginBottom: 5,
                      color: 'black',
                    }}>
                    Order ID {item.id}
                  </Text>
                  <Text style={{marginBottom: 5, fontSize: 12, color: 'blue'}}>
                    This order will finish on {item.estimateFinish}
                  </Text>
                </View>

                <View>
                  <View
                    style={{
                      marginHorizontal: 15,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={{fontWeight: 'bold'}}>Service</Text>
                      <Text style={{marginBottom: 5}}>
                        {item.servicelist.services}
                      </Text>
                    </View>

                    {item.weight == 0 ? (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{fontWeight: 'bold'}}>Items</Text>
                        <Text style={{marginBottom: 5}}>
                          {item.item[0].name} {item.item[0].quantity}
                        </Text>
                      </View>
                    ) : (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{fontWeight: 'bold'}}>Weight</Text>
                        <Text style={{marginBottom: 5}}>{item.weight} kg</Text>
                      </View>
                    )}

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={{fontWeight: 'bold'}}>Status</Text>
                      <Text style={{marginBottom: 10}}>
                        {item.orderstatus.status}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    height: 50,
    marginTop: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  search: {
    width: '80%',
    height: 36,
    marginTop: 7,
    backgroundColor: 'white',
    color: 'black',
    borderRadius: 10,
    paddingHorizontal: 35,
    position: 'absolute',
  },

  logo: {
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    height: 266,
    position: 'absolute',
    flex: 1,
  },
  logoNotif: {
    width: '20%',
    flexDirection: 'row',
    height: 36,
    position: 'absolute',
    flex: 1,
  },

  Cart: {
    width: 100,
    height: 100,
    aspectRatio: 1,
    // marginBottom: 50,
    // marginLeft: 20,
    top: 250,
  },
  textCart: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 20,
    top: 250,
  },
  Process: {
    fontWeight: 'bold',
    color: 'white',
    marginTop: 20,
    marginBottom: 10,
    marginHorizontal: 30,
    fontSize: 20,
  },
  Process2: {
    fontWeight: 'bold',
    color: 'white',
    marginTop: 20,
    marginBottom: 10,
    marginHorizontal: 30,
    fontSize: 20,
    borderBottomColor: 'white',
    borderBottomWidth: 3,
  },
});
export default orderPage;
