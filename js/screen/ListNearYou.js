import React from 'react';
import {
  StyleSheet,
  Image,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ImageBackground,
  FlatList,
} from 'react-native';
import BGHome from '../assets/BGHome.png';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SearchBar from './Component/Search/index';
import CardListNearYou from './Component/CardListNearYou/index';
import {useDispatch, useSelector} from 'react-redux';

const ListNearYou = ({navigation}) => {
  const {AllLaundry} = useSelector(state => state.AllLaundryReducers);
  const dispatch = useDispatch();

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGHome} />

      {/* header */}
      <View style={styles.header}>
        <TouchableOpacity>
          <AntDesign
            style={{marginRight: 10}}
            name="left"
            size={20}
            color={'white'}
            onPress={() => navigation.goBack()}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{width: '90%'}}
          onPress={() => navigation.navigate('SearchPage')}>
          <SearchBar />
        </TouchableOpacity>
        <View style={{marginLeft: 10}}></View>
      </View>

      {/* body */}
      <Text style={styles.namePage}>Laundry List</Text>

      {/* Card List Near You */}
      <FlatList
        showsVerticalScrollIndicator={false}
        data={AllLaundry}
        renderItem={props => {
          return (
            <View style={{marginBottom: 10}}>
              <CardListNearYou
                navigation={navigation}
                id={props.item.id}
                laundryname={props.item.name}
                rating={props.item.average_rating}
                location={props.item.address.street}
                totalservices={props.item.total_services}
                pickup={
                  props.item.pickUpAndDelivery == true
                    ? 'Pick Up & Delivery'
                    : 'Pick Up & Delivery not supported'
                }
              />
            </View>
          );
        }}
      />
    </View>
  );
};

export default ListNearYou;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    height: 50,
    marginTop: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  search: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 7,
  },
  namePage: {
    fontWeight: 'bold',
    color: 'white',
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 10,
    fontSize: 20,
  },
  logoMenu: {
    width: 100,
    height: null,
    aspectRatio: 1,
  },

  titleList: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  logo: {
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    height: 266,
    // top: 7,
    position: 'absolute',
    flex: 1,
  },
  logoNotif: {
    width: '20%',
    flexDirection: 'row',
    // alignContent: 'center',
    height: 36,
    position: 'absolute',
    flex: 1,
  },
  nearYou: {
    fontWeight: 'bold',
    color: 'black',
    marginHorizontal: 20,
    marginTop: 30,
    marginBottom: 10,
    fontSize: 20,
  },
});
