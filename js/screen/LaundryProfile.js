import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Text,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BGHome from '../assets/BGHome.png';
import SearchBar from '../screen/Component/Search/index';
import LaundryProfileCard from '../screen/Component/LaundryProfile/index';
import TermsCard from '../screen/Component/TermsCard/index';
import {useSelector} from 'react-redux';

const LaundryProfile = props => {
  const {detail} = useSelector(state => state.AllLaundryReducers);
  console.log('isi detail', detail);
  const [servicesMenu, setServicesMenu] = useState('');

  const selectServices = id => {
    setServicesMenu(id);
  };

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGHome}></ImageBackground>

      {/* header */}
      <View style={styles.header}>
        <TouchableOpacity>
          <AntDesign
            style={{marginRight: 10}}
            name="left"
            size={20}
            color={'white'}
            onPress={() => props.navigation.goBack()}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{width: '90%'}}
          onPress={() => navigation.navigate('SearchPage')}>
          <SearchBar />
        </TouchableOpacity>
        <View style={{marginLeft: 10}}></View>
      </View>

      {/* body */}
      <View style={{marginBottom: 20}}>
        <View style={{marginBottom: 10}}>
          <LaundryProfileCard
            navigation={props.navigation}
            id={detail.id}
            laundryname={detail.name}
            rating={detail.average_rating}
            location={detail.address?.street}
            number={detail.mobile_phone}
            pickup={
              detail.pickUpAndDelivery === true
                ? 'Pick Up and Delivery'
                : 'Pick Up and Delivery not supported'
            }
          />
        </View>
        <View style={styles.TermsCondition}>
          <TermsCard />
        </View>
      </View>

      <View
        style={{
          paddingHorizontal: 20,
          flex: 1,
          paddingBottom: 10,
        }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={detail.services}
          keyExtractor={item => item.id}
          renderItem={({item}) => {
            return (
              <View>
                <View>
                  {/* title */}
                  <View style={{marginVertical: 10}}>
                    <Text style={{fontWeight: 'bold'}}>{item.name}</Text>
                  </View>
                  {/* content */}
                  <TouchableOpacity
                    onPress={() =>
                      props.navigation.navigate('InputOrderPage', {
                        detail: item,
                        a: detail,
                      })
                    }>
                    <View
                      style={{
                        flexDirection: 'row',
                        padding: 20,
                        borderColor: '#EFEFEF',
                        borderWidth: 1,
                        borderRadius: 10,
                      }}>
                      <View>
                        <Image
                          style={{width: 50, height: 50}}
                          source={{
                            uri:
                              'https://washme.gabatch11.my.id/' +
                              item.basic.image,
                          }}
                        />
                      </View>
                      <View
                        style={{
                          flex: 1,
                          marginLeft: 10,
                          justifyContent: 'space-around',
                        }}>
                        <Text>
                          {item.name} {item.basic.name}
                        </Text>
                        <View>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text style={{fontSize: 12}}>4 days working</Text>
                            <Text style={{fontSize: 12}}>
                              Rp {item.basic.byWeight?.price}/kg
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text style={{fontSize: 12}}>
                              Pick up & Delivery
                            </Text>
                            <Text style={{fontSize: 12}}>
                              Rp {item.basic.byItem.minimumPrice}/item
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>

                    {/* Express */}
                    {item.express == undefined ? null : (
                      <View
                        style={{
                          flexDirection: 'row',
                          padding: 20,
                          borderColor: '#EFEFEF',
                          borderWidth: 1,
                          borderRadius: 10,
                        }}>
                        <View>
                          <Image
                            style={{width: 50, height: 50}}
                            source={{
                              uri:
                                'https://washme.gabatch11.my.id/' +
                                item.express?.image,
                            }}
                          />
                        </View>

                        <View
                          style={{
                            flex: 1,
                            marginLeft: 10,
                            justifyContent: 'space-around',
                          }}>
                          <Text>
                            {item.name}
                            {item.express?.name}
                          </Text>
                          <View>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                              }}>
                              <Text style={{fontSize: 12}}>4 days working</Text>
                              <Text style={{fontSize: 12}}>
                                Rp {item.express?.byWeight.price}/kg
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                              }}>
                              <Text style={{fontSize: 12}}>
                                Pick up & Delivery
                              </Text>
                              <Text style={{fontSize: 12}}>
                                Rp {item.express?.byItem.minimumPrice}/item
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    )}
                  </TouchableOpacity>
                </View>
              </View>
            );
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    height: 50,
    marginTop: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  search: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 7,
  },
  logo: {
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    height: 266,
    position: 'absolute',
    flex: 1,
  },
  itemServices: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    fontWeight: 'bold',
    fontSize: 16,
  },
  TermsCondition: {
    marginHorizontal: 10,
    alignItems: 'center',
  },
});

export default LaundryProfile;
