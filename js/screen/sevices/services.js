import React from 'react';
import {
  StyleSheet,
  Image,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ImageBackground,
  FlatList,
} from 'react-native';
import BGHome from '../../assets/BGHome.png';
import AntDesign from 'react-native-vector-icons/AntDesign';
import CardListNearYou from '../Component/CardListNearYou/index';
import SearchBar from '../Component/Search/index';
import {useDispatch, useSelector} from 'react-redux';
import {GetDetailLaundry} from '../../Redux/action/ActionMenu';

export default function services({navigation}) {
  const {AllLaundry, data} = useSelector(state => state.AllLaundryReducers);
  console.log('ini data services', data);
  const dispatch = useDispatch();

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGHome} />

      {/* header */}
      <View style={styles.header}>
        <TouchableOpacity>
          <AntDesign
            style={{marginRight: 10}}
            name="left"
            size={20}
            color={'white'}
            onPress={() => navigation.goBack()}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{width: '90%'}}
          onPress={() => props.navigation.navigate('SearchPage')}>
          <SearchBar />
        </TouchableOpacity>
        <View style={{marginLeft: 10}}></View>
      </View>

      {/* body */}
      <Text style={styles.nameService}>{data.nameService}</Text>

      <FlatList
        showsVerticalScrollIndicator={false}
        data={data.data}
        keyExtractor={item => item.id}
        renderItem={({item}) => {
          return (
            <View style={{marginBottom: 10}}>
              <CardListNearYou
                navigation={navigation}
                id={item.id}
                laundryname={item.name}
                rating={item.average_rating}
                location={item.address.street}
                totalservices={item.total_services}
                pickup={
                  item.pickUpAndDelivery == true
                    ? 'Pick Up & Delivery'
                    : 'Pick Up & Delivery not supported'
                }
              />
            </View>
          );
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    height: 50,
    marginTop: 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  search: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 7,
  },
  nameService: {
    fontWeight: 'bold',
    color: 'white',
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 10,
    fontSize: 20,
  },
  logoMenu: {
    width: 100,
    height: null,
    aspectRatio: 1,
  },

  titleList: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  logo: {
    width: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    height: 266,
    // top: 7,
    position: 'absolute',
    flex: 1,
  },
  logoNotif: {
    width: '20%',
    flexDirection: 'row',
    // alignContent: 'center',
    height: 36,
    position: 'absolute',
    flex: 1,
  },
  nearYou: {
    fontWeight: 'bold',
    color: 'black',
    marginHorizontal: 20,
    marginTop: 30,
    marginBottom: 10,
    fontSize: 20,
  },
});
