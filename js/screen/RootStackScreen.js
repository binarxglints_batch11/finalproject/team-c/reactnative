import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// Screen
import splashScreen from './landingPage/splashScreen';
import loginPage from './loginPage';
import registerPage from './registerPage';
import landingGetStarted from './landingPage/landingGetStarted';
import MainTab from './MainTab';
import orderPage from '../screen/orderPage';
import ListNearYou from './ListNearYou';
import listPage from './listPage';
import services from './sevices/services';
import LaundryProfile from '../screen/LaundryProfile';
import SearchPage from '../screen/SearchPage';
import SearchResult from '../screen/SearchResult';
import InputOrderPage from '../screen/InputOrderPage';
import InputAddressPage from '../screen/InputAddressPage';
import PaymentPage from '../screen/PaymentPage';
import OrderDetailPage from '../screen/OrderDetailPage';

const RootStack = createStackNavigator();

const RootStackScreen = ({}) => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="splashScreen" component={splashScreen} />
    <RootStack.Screen name="landingGetStarted" component={landingGetStarted} />
    <RootStack.Screen name="registerPage" component={registerPage} />
    <RootStack.Screen name="loginPage" component={loginPage} />
    <RootStack.Screen name="MainTab" component={MainTab} />
    <RootStack.Screen name="orderPage" component={orderPage} />
    <RootStack.Screen name="ListNearYou" component={ListNearYou} />
    <RootStack.Screen name="listPage" component={listPage} />
    <RootStack.Screen name="services" component={services} />
    <RootStack.Screen name="SearchPage" component={SearchPage} />
    <RootStack.Screen name="SearchResult" component={SearchResult} />
    <RootStack.Screen name="LaundryProfile" component={LaundryProfile} />
    <RootStack.Screen name="InputOrderPage" component={InputOrderPage} />
    <RootStack.Screen name="InputAddressPage" component={InputAddressPage} />
    <RootStack.Screen name="PaymentPage" component={PaymentPage} />
    <RootStack.Screen name="OrderDetailPage" component={OrderDetailPage} />
  </RootStack.Navigator>
);

export default RootStackScreen;
