import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
  ImageBackground,
  Pressable,
} from 'react-native';
import Button from '../screen/Component/Button/index';
import BGLogin from '../assets/BGLogin.png';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const registerPage = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const RegisterAccess = () => {
    var FormData = require('form-data');
    var data = new FormData();
    data.append('email', email);
    data.append('password', password);

    var config = {
      method: 'post',
      url: 'https://washme.gabatch11.my.id/register/customer',

      data: data,
    };

    // axios(config)
    // .then(function ({data}) {
    //   AsyncStorage.setItem('token', data.token, err => {
    //     if (!err) {
    //       console.log('register');
    //       setEmail('');
    //       setPassword('');
    //       navigation.navigate('loginPage');
    //     }
    //   });
    // })
    // .catch(function (error) {
    //   alert(error);
    // });

    axios(config)
      .then(function ({data}) {
        console.log('dataregister', data);

        if (data) {
          setEmail('');
          setPassword('');
          navigation.navigate('loginPage');
        }
      })
      .catch(function (error) {
        alert(error);
      });
  };

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGLogin}>
        <Text style={styles.textHeader}>
          One more step to make your order! ✨
        </Text>
        <Text style={styles.textSubHeader}>
          Sign up to book your laundry services
        </Text>
      </ImageBackground>
      <Text style={styles.tag}>E-mail</Text>
      <TextInput
        style={styles.inputText}
        onChangeText={setEmail}
        value={email}
        placeholder="Type your name"
        placeholderTextColor="#D6D6D6"
      />
      <Text style={styles.tag}> Create Password</Text>
      <TextInput
        style={styles.inputText}
        placeholder="Password"
        placeholderTextColor="#D6D6D6"
        secureTextEntry={true}
        onChangeText={setPassword}
        value={password}
      />
      <View style={styles.button}>
        <Button textButton="Sign Up" onPressButton={RegisterAccess} />
      </View>

      <Text style={styles.textAccount}>
        Already have an account ?{' '}
        <Text
          style={{fontWeight: 'bold', fontSize: 13, color: '#FF6C6C'}}
          onPress={() => navigation.navigate('loginPage')}>
          Sign In here
        </Text>
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: '100%',
    height: null,
    aspectRatio: 1.5,
    marginBottom: 25,
    paddingTop: 125,
  },
  tag: {
    color: '#455D6E',
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 5,
    width: '90%',
  },
  inputText: {
    width: '90%',
    borderRadius: 7,
    padding: 10,
    marginBottom: 10,
    borderColor: '#ECECEC',
    borderWidth: 2,
  },
  textHeader: {
    borderRadius: 7,
    marginBottom: 10,
    color: '#FF6C6C',
    marginHorizontal: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  textSubHeader: {
    color: '#A0A0A0',
    marginHorizontal: 20,
  },
  textAccount: {
    color: '#A0A0A0',
    marginTop: 15,
    marginBottom: 10,
    fontSize: 13,
  },
  button: {
    width: '90%',
    height: '20%',
    justifyContent: 'center',
  },
});

export default registerPage;
