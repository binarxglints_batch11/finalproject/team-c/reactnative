import React from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import MainMenuShoes from '../../../assets/MainMenuShoes.png';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default function ShoesProfileCard() {
  const navigation = useNavigation();
  console.log(navigation);
  return (
    <View>
      {/* title */}
      <View style={{marginVertical: 20}}>
        <Text style={{fontWeight: 'bold'}}>Shoes</Text>
      </View>

      {/* content */}
      <TouchableOpacity
        onPress={() => navigation.navigate('InputOrderPage')}
        style={{
          flexDirection: 'row',
          padding: 20,
          borderColor: '#EFEFEF',
          borderWidth: 1,
          borderRadius: 10,
        }}>
        <View>
          <Image source={MainMenuShoes} />
        </View>
        <View style={{flex: 1, marginLeft: 10, justifyContent: 'space-around'}}>
          <Text>Shoes</Text>
          <View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 12}}>4 days working</Text>
              <Text style={{fontSize: 12}}>Rp. 15.000/Kg</Text>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{fontSize: 12}}>Pick up & Delivery</Text>
              <Text style={{fontSize: 12}}>Rp. 8.000/item</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}
