import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Pressable,
} from 'react-native';

const Button = props => {
  return (
    <View style={{flex: 1}}>
      <TouchableOpacity style={styles.Button2} onPress={props.onPressButton}>
        <Text style={styles.textButton2}>{props.textButton}</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  Button: {
    width: '90%',
    borderRadius: 7,
    alignItems: 'center',
    padding: 10,
    // marginTop: 25,
    backgroundColor: 'white',
    borderWidth: 1,
  },
  Button2: {
    width: '100%',
    borderRadius: 7,
    alignItems: 'center',
    padding: 10,
    marginTop: 25,
    backgroundColor: '#FF6C6C',
    borderWidth: 1,
  },
  textButton: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
  },
  textButton2: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
export default Button;
