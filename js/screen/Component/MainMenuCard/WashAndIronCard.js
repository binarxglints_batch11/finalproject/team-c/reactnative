import React from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import MainMenuWashAndIron from '../../../assets/MainMenuWashAndIron.png';
import ProfilePickupDriver from '../../../assets/ProfilePickupDriver.png';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {GetDetailLaundry} from '../../../Redux/action/ActionMenu';
import {useDispatch} from 'react-redux';

export default function WashAndIronCard(props) {
  const dispatch = useDispatch();
  console.log('ini props', props);
  return (
    <View style={{alignItems: 'center', marginBottom: 10}}>
      <TouchableOpacity
        onPress={() => {
          dispatch(GetDetailLaundry(props.id)),
            props.navigation.navigate('LaundryProfile');
        }}
        style={{
          // backgroundColor: 'yellow',
          backgroundColor: 'white',
          height: 150,
          width: 320,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
          borderRadius: 10,
        }}>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <Image
            style={{
              marginTop: 40,
              aspectRatio: 1,
              marginLeft: 10,
            }}
            source={MainMenuWashAndIron}
          />
          <View style={{backgroundColor: 'white', marginHorizontal: 15}}>
            <View
              style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginRight: 10,
                marginTop: 10,
                marginBottom: 5,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontWeight: 'bold',
                  marginRight: 10,
                  fontSize: 16,
                }}>
                {props.laundryname}
              </Text>
              <Text
                style={{
                  color: 'black',
                  marginRight: 40,
                }}>
                {props.distance}
              </Text>
              <Text
                style={{
                  color: 'black',
                  marginRight: 10,
                }}>
                <AntDesign name="star" size={15} color={'orange'} />
                {props.rating}
              </Text>
            </View>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={{marginBottom: 15}}>{props.services}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={{marginRight: 5}}>{props.time} days working</Text>
                <Text style={{fontWeight: 'bold', marginBottom: 5}}>
                  Rp {props.price}/kg
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Image
                  style={{
                    width: 12,
                    height: 12,
                    marginTop: 5,

                    //   marginLeft: 15,
                    // backgroundColor: 'black',
                  }}
                  source={ProfilePickupDriver}
                />
                <Text style={{marginRight: 10}}>{props.pickup}</Text>
                <Text style={{marginBottom: 10, fontWeight: 'bold'}}>
                  Rp {props.pricePickup}/item
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}
