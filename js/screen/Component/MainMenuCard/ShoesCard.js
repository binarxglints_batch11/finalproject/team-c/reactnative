import React from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import MainMenuShoes from '../../../assets/MainMenuShoes.png';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default function ShoesCard(props) {
  console.log(props);
  return (
    <View style={{alignItems: 'center', marginBottom: 10}}>
      <View
        style={{
          // backgroundColor: 'yellow',
          backgroundColor: 'white',
          height: 150,
          width: 320,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
          borderRadius: 10,
        }}>
        <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
          <Image
            style={{
              width: 50,
              height: 80,
              marginTop: 40,
              aspectRatio: 1,
              marginLeft: 10,
              marginRight: 10,
            }}
            source={MainMenuShoes}
          />
          <View>
            <View
              style={{
                // backgroundColor: 'green',
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginRight: 15,
                marginTop: 15,
                marginBottom: 5,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontWeight: 'bold',
                  marginRight: 10,
                  fontSize: 16,
                }}>
                {props.laundryname}
              </Text>
              <Text
                style={{
                  color: 'black',
                  marginRight: 40,
                }}>
                {props.distance} km
              </Text>
              <Text
                style={{
                  color: 'black',
                  marginRight: 10,
                }}>
                <AntDesign name="star" size={15} color={'orange'} />
                {props.rating}
              </Text>
            </View>
            <View
              style={{
                // backgroundColor: 'blue',
                marginRight: 15,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={{marginBottom: 15}}>{props.services}</Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text>{props.time} days working</Text>
                <Text style={{fontWeight: 'bold', marginBottom: 5}}>
                  Rp {props.price}/kg
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text>{props.pickup}</Text>
                <Text style={{marginBottom: 10, fontWeight: 'bold'}}>
                  Rp {props.pricePickup}/item
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}
