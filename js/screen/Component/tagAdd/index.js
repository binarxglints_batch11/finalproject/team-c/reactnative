import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Touchable,
  Image,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
export default function TagAdd(props) {
  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          style={{
            width: '60%',
            padding: 10,
            justifyContent: 'space-between',
            flexDirection: 'row',
            marginRight: 10,
          }}>
          <Text style={{marginVertical: 10, fontWeight: 'bold', fontSize: 18}}>
            Clothes Detail
          </Text>
          <AntDesign
            name="down"
            size={20}
            color={'black'}
            style={{backgroundColor: 'white'}}
          />
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',

            flex: 1,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={props.handleCountMinus}
            style={{flex: 1, alignItems: 'center', backgroundColor: '#FF415B'}}>
            <Text style={{fontSize: 30}}>-</Text>
          </TouchableOpacity>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{textAlign: 'center'}}>{props.count}</Text>
          </View>
          <TouchableOpacity
            onPress={props.handleCountPlus}
            style={{flex: 1, alignItems: 'center', backgroundColor: '#FF415B'}}>
            <Text style={{fontSize: 30}}>+</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});
