import React from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import ImageLaundry from '../../../assets/ImageLaundry.png';
import AntDesign from 'react-native-vector-icons/AntDesign';
import ProfilePickupDriver from '../../../assets/ProfilePickupDriver.png';
import {useDispatch} from 'react-redux';
import {GetDetailLaundry} from '../../../Redux/action/ActionMenu';

export default function CardListNearYou(props) {
  const dispatch = useDispatch();

  return (
    <View style={{alignItems: 'center', marginBottom: 10}}>
      <TouchableOpacity
        onPress={async () => {
          await dispatch(GetDetailLaundry(props.id)),
            await props.navigation.navigate('LaundryProfile');
        }}>
        <View
          style={{
            // backgroundColor: 'pink',
            backgroundColor: 'white',
            height: 150,
            width: 320,

            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,

            elevation: 2,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
            borderRadius: 10,
          }}>
          <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
            <Image
              style={{
                width: 100,
                height: 85,
                aspectRatio: 1,
                marginTop: 15,
                marginLeft: 15,
                // backgroundColor: 'black',
              }}
              source={ImageLaundry}
            />
            <View>
              <View
                style={{
                  //   backgroundColor: 'green',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginRight: 15,
                  marginTop: 15,
                  marginBottom: 5,
                }}>
                <Text
                  style={{
                    color: 'black',
                    fontWeight: 'bold',
                    marginRight: 10,
                    fontSize: 16,
                  }}>
                  {props.laundryname}
                </Text>
                <Text
                  style={{
                    color: 'black',
                    marginRight: 10,
                  }}>
                  <AntDesign name="star" size={15} color={'orange'} />
                  {props.rating}
                </Text>
              </View>
              <View
                style={{
                  //   backgroundColor: 'blue',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginRight: 15,
                  marginTop: 10,
                  marginBottom: 5,
                }}>
                <Text
                  style={{
                    color: 'black',
                    marginRight: 10,
                  }}>
                  {props.location}
                </Text>
                <Text
                  style={{
                    color: 'black',
                    marginRight: 10,
                  }}>
                  {props.distance}
                </Text>
              </View>
              <View
                style={{
                  //   backgroundColor: 'yellow',
                  marginRight: 15,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={{paddingRight: 30}}>
                    {props.totalservices} services
                  </Text>
                  <Text style={{marginBottom: 5}}>{props.infoservices}</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Image
                    style={{
                      width: 12,
                      height: 12,
                      marginTop: 5,

                      //   marginLeft: 15,
                      // backgroundColor: 'black',
                    }}
                    source={ProfilePickupDriver}
                  />
                  <Text style={{paddingRight: 30}}>{props.pickup}</Text>
                  <Text style={{marginBottom: 10}}>{props.infopickup}</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}
