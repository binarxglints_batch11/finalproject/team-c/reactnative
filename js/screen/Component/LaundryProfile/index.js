import React from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import ImageLaundry from '../../../assets/ImageLaundry.png';
import AntDesign from 'react-native-vector-icons/AntDesign';
import ProfilePickupDriver from '../../../assets/ProfilePickupDriver.png';
import ProfileLocation from '../../../assets/ProfileLocation.png';
import ProfileNumber from '../../../assets/ProfileNumber.png';

export default function LaundryProfileCard(props) {
  return (
    <TouchableOpacity onPress={props.navigation.navigate('LaundryProfile')}>
      <View style={{alignItems: 'center', marginBottom: 10}}>
        <View
          style={{
            backgroundColor: 'white',
            width: 320,
            height: 145,
            top: 20,

            borderRadius: 10,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
            elevation: 2,
          }}>
          <View>
            <View
              style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginHorizontal: 10,
                marginTop: 15,
                marginBottom: 5,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontWeight: 'bold',
                  marginRight: 10,
                  fontSize: 16,
                }}>
                {props.laundryname}
              </Text>
              <Text
                style={{
                  color: 'black',
                  marginRight: 10,
                }}>
                <AntDesign name="star" size={15} color={'orange'} />
                {props.rating}
              </Text>
            </View>

            <View
              style={{
                marginLeft: 10,
                justifyContent: 'space-between',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={{flexDirection: 'column'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{
                      width: 13,
                      height: 14,
                      marginBottom: 5,
                      marginRight: 10,
                      resizeMode: 'stretch',
                    }}
                    source={ProfileLocation}
                  />
                  <Text style={{paddingRight: 30, marginBottom: 5}}>
                    {props.location}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Image
                    style={{
                      width: 12,
                      height: 12,
                      marginTop: 5,
                      marginBottom: 5,
                      marginRight: 10,
                    }}
                    source={ProfileNumber}
                  />
                  <Text style={{marginBottom: 5}}>{props.number}</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Image
                    style={{
                      width: 12,
                      height: 12,
                      marginTop: 5,
                      marginBottom: 5,
                      marginRight: 10,
                    }}
                    source={ProfilePickupDriver}
                  />
                  <Text style={{paddingRight: 30}}>{props.pickup}</Text>
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'space-between',
                }}>
                <Image
                  style={{
                    width: 85,
                    height: 85,

                    marginRight: 20,
                    resizeMode: 'stretch',
                  }}
                  source={ImageLaundry}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}
