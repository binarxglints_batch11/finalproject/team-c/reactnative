import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import MainMenuWashAndIron from '../../../assets/MainMenuWashAndIron.png';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default function LaundryCard(props) {
  const [radioButton, setRadioButton] = useState(false);

  const handleradioButton = item => {
    setRadioButton(!radioButton);
  };

  return (
    <TouchableOpacity style={{marginLeft: 5}}>
      <View
        style={{
          backgroundColor: 'white',
          marginHorizontal: 20,
          borderRadius: 10,
          height: 150,
          width: 320,
          shadowColor: '#000',
          shadowOffset: {
            width: 1,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
        }}>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
            marginTop: 5,
            marginHorizontal: 15,
            marginBottom: 15,
            marginTop: 15,
            borderBottomWidth: 1,
            borderBottomColor: 'gray',
          }}>
          {radioButton === false ? (
            <TouchableOpacity
              style={styles.radioButton}
              onPress={handleradioButton}>
              <MaterialIcons name="circle" size={30} color={'white'} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.radioButton}
              onPress={handleradioButton}>
              <MaterialIcons
                name="circle"
                size={30}
                color={'blue'}
                style={{backgroundColor: 'black', borderRadius: 20}}
              />
            </TouchableOpacity>
          )}
          <Text
            style={{
              color: 'black',
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            {props.laundryname}
          </Text>
          <Text
            style={{
              borderWidth: 1,
              borderColor: 'blue',
              borderRadius: 10,
              marginBottom: 10,
              color: 'blue',
            }}>
            {'   '}See Services{'   '}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <View>
            <Image
              style={{
                width: 100,
                height: 50,
                aspectRatio: 1,
                marginLeft: 15,
                marginBottom: 10,
              }}
              source={MainMenuWashAndIron}
            />
          </View>
          {/* </TouchableOpacity> */}
          <View
            style={{
              flexDirection: 'column',
              marginRight: 10,
            }}>
            <Text style={{fontWeight: 'bold'}}>{props.services}</Text>
            <Text>Weight: {props.weight} kg</Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              marginRight: 15,
            }}>
            <Text style={{fontWeight: 'bold'}}>Rp {props.price}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  radioButton: {
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 20,
  },
});
