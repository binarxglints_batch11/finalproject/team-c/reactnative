import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const SearchBar = () => {
  return (
    <View style={styles.search}>
      <AntDesign name="search1" size={25} color={'gray'} />
      <Text
        style={{
          marginLeft: 5,
        }}>
        Search
      </Text>
    </View>
  );
};
export default SearchBar;
const styles = StyleSheet.create({
  search: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 7,
    width: 295,
    height: 36,
  },
});
