import React, {useEffect, useState} from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import ImageLaundry from '../../../assets/ImageLaundry.png';

export default function CardNearYou(props) {
  return (
    <TouchableOpacity
      onPress={() => {
        props.navigation.navigate('ListNearYou');
      }}
      style={{alignItems: 'center', marginLeft: 20}}>
      <View
        style={{
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
          borderBottomLeftRadius: 2,
          borderBottomRightRadius: 2,
        }}>
        <Image
          style={{
            width: 150,
            height: null,
            aspectRatio: 1.3,
            borderRadius: 10,
            marginBottom: 5,
          }}
          source={ImageLaundry}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingHorizontal: 5,
          }}>
          <Text style={{fontWeight: 'bold'}}>{props.laundryname}</Text>
          <View style={{flexDirection: 'row'}}>
            <AntDesign name="star" size={20} color={'orange'} />
            <Text style={{fontWeight: 'bold'}}>{props.rating}</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 5,
            marginBottom: 5,
            paddingHorizontal: 5,
          }}>
          <AntDesign name="enviroment" size={20} color={'gray'} />
          <Text style={{marginLeft: 5}}>{props.location}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
