import React from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native'
import { useDispatch } from 'react-redux';
import { GetDetailLaundry, GetOnProcess } from '../../../Redux/action/ActionMenu';
// import MainMenuWashOnly from '../../../assets/MainMenuWashOnly.png';

export default function OrderCard(props) {
  const navigation = useNavigation()
  console.log('OnProsesPage', navigation);
  
  


  return (
    <TouchableOpacity
      style={{alignItems: 'center', marginBottom: 10}}
      onPress={() => navigation.navigate('OrderDetailPage')}>
      <View
        style={{
          backgroundColor: 'white',
          width: 300,
          height: 180,

          marginRight: 5,
          borderRadius: 10,
          shadowColor: '#000',
          shadowOffset: {
            width: 1,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
        }}>
        <View
          style={{
            marginTop: 10,
            marginBottom: 10,
            marginHorizontal: 15,
            flexDirection: 'column',
            borderBottomColor: 'gray',
            borderBottomWidth: 1,
          }}>
          <Text style={{fontWeight: 'bold', marginTop: 10, marginBottom: 5}}>
            {props.laundryname}
          </Text>
          <Text style={{fontWeight: 'bold', marginBottom: 5, color: 'black'}}>
            Order ID {props.orderID}
          </Text>
          <Text style={{marginBottom: 5, fontSize: 12, color: 'blue'}}>
            This order will finish on {props.date}
          </Text>
        </View>

        <View>
          <View
            style={{
              marginHorizontal: 15,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{fontWeight: 'bold'}}>Service</Text>
              <Text style={{marginBottom: 5}}>{props.service}</Text>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{fontWeight: 'bold'}}>Weight</Text>
              <Text style={{marginBottom: 5}}>{props.weight} kg</Text>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{fontWeight: 'bold'}}>Status</Text>
              <Text style={{marginBottom: 10}}>{props.status}</Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}
