import React from 'react';
import {View, Text, Image, ImageBackground} from 'react-native';

export default function TermsCard(props) {
  // console.log(props);
  return (
    <View style={{alignItems: 'center', marginTop: 10}}>
      <View
        style={{
          // backgroundColor: 'yellow',
          backgroundColor: 'white',
          width: 320,
          height: 80,

          marginRight: 5,
          borderRadius: 10,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,

          elevation: 2,
        }}>
        <View
          style={{
            marginTop: 10,
            marginBottom: 10,
            marginHorizontal: 15,
            flexDirection: 'column',
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              marginTop: 10,
              marginBottom: 5,
              marginHorizontal: 5,
              textAlign: 'center',
            }}>
            Read the terms of ABC Laundry here
          </Text>
          <Text
            style={{
              fontSize: 12,
              marginBottom: 10,
              color: 'black',
              marginHorizontal: 5,
              textAlign: 'center',
            }}>
            Read this first so that your order will run smoothly
          </Text>
        </View>
      </View>
    </View>
  );
}
