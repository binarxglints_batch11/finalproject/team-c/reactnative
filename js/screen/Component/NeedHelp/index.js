import React from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import NeedHelp from '../../../assets/NeedHelp.png';

export default function CardNeedHelp(props) {
  return (
    <View style={{alignItems: 'center'}}>
      <TouchableOpacity
        style={{marginLeft: 20}}
        onPress={() => props.navigation.navigate('')}>
        <View>
          <Image
            style={{
              width: 124,
              height: 24,
              marginBottom: 15,
            }}
            source={NeedHelp}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}
