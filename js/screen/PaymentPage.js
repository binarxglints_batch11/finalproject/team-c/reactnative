import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Location from '../assets/Location.png';
import DistanceLocation from '../assets/DistanceLocation.png';
import ProfileNumber from '../assets/ProfileNumber.png';
import MainMenuWashAndIron from '../assets/MainMenuWashAndIron.png';
import {ScrollView} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import {GetOnProcess, PostCreateOrder} from '../Redux/action/ActionMenu';
import {useNavigation} from '@react-navigation/native';

export default function PaymentPage(props) {
  const navigation = useNavigation();
  console.log('Paymentpage', props);

  const dispatch = useDispatch();

  //Payment
  const [dDPayment, setdDPayment] = useState(false);
  const [paymentId, setPaymentId] = useState('');
  const [deliveryFee, setDeliveryFee] = useState(12000);
  const DeliveryFee = deliveryFee;
  const [adminCharge, setAdminCharge] = useState(7000);
  const AdminCharge = adminCharge;
  const [pickupChoice, setPickupChoice] = useState('Delivery');
  const PickUpChoice = pickupChoice;

  const handleDropDownPayment = () => {
    setdDPayment(!dDPayment);
  };
  const payment = [
    {id: '1', value: 'Cash'},
    {id: '2', value: 'E-wallet'},
    {id: '3', value: 'Debit/credit card'},
  ];
  const [paymentName, setPaymentName] = useState('Select Payment');

  const handlePayment = value => {
    setPaymentName(value.value);
    setdDPayment(!dDPayment);
  };

  //Data
  const Payment = paymentName;
  const DataToko = props.route.params.DataDetailToko;
  const DataLaundry = props.route.params.DataOrder;
  const PriceOrder = props.route.params.PriceOrder;
  const TotalPrice = props.route.params.PriceFinal;
  const Address = props.route.params.Address;

  //Conversi id
  const servicesName =
    DataLaundry.servicesId == 1
      ? 'Wash & Iron'
      : DataLaundry.servicesId == 2
      ? 'Wash'
      : DataLaundry.servicesId == 3
      ? 'Iron'
      : DataLaundry.servicesId == 4
      ? 'Dry Clean'
      : DataLaundry.servicesId == 5
      ? 'Shoes'
      : 'Household';
  const levelName = DataLaundry.levelServices == 1 ? 'Basic' : 'Express';
  const typesName = DataLaundry.typesId == '1' ? 'By Item' : 'By Weight';
  const ItemClothes = DataLaundry.ItemClothes;

  //variabel
  const laundry_id = DataToko.id;
  const ordertype_id = DataLaundry.typesId.toString();
  const servicelist_id = DataLaundry.servicesId.toString();
  const servicelevel_id = DataLaundry.levelServices.toString();
  const weight = DataLaundry.countWeight.toString();
  const item = DataLaundry.ItemClothes;
  const estimatePrice = PriceOrder.toString();
  const pickUpChoice = PickUpChoice;
  const pickUpAddress = {
    address: Address.addressCustomer,
    note: Address.notePickUp,
  };

  const deliveryAddress = {
    address: Address.addressDelivery,
    note: Address.noteDelivery,
  };
  const delivery_fee = DeliveryFee.toString();
  const admin_charge = AdminCharge.toString();
  const totalPrice = TotalPrice.toString();
  const paymentType = Payment;

  const PaymentDetail = {
    laundry_id,
    ordertype_id,
    servicelist_id,
    servicelevel_id,
    weight,
    item: JSON.stringify(item),
    estimatePrice,
    pickUpChoice,
    pickUpAddress: JSON.stringify(pickUpAddress),
    deliveryAddress: JSON.stringify(deliveryAddress),
    delivery_fee,
    admin_charge,
    totalPrice,
    paymentType,
  };
  console.log('paymentDetail', PaymentDetail);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <AntDesign
          name="left"
          size={20}
          color={'white'}
          onPress={() => props.navigation.goBack()}
        />
        <Text style={styles.titlePage}>Summary</Text>
        <Text></Text>
      </View>

      {/* delivery */}
      <ScrollView>
        <Text style={styles.subBodyContent}>Delivery</Text>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image style={styles.imageLoc} source={Location} />
          </View>
          <View style={styles.cardAddress}>
            <Text style={styles.textAddressInfo}>Pick up at</Text>
            <Text>{Address.addressCustomer}</Text>
            <Text style={{fontSize: 12}}>{Address.notePickUp}</Text>
          </View>
        </View>
        <View>
          <Image style={styles.imageDistanceLoc} source={DistanceLocation} />
        </View>
        <View
          style={{
            // backgroundColor: 'pink',
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image style={styles.imageLoc} source={Location} />
          </View>
          <View style={styles.cardAddress}>
            <Text style={styles.textAddressInfo}>Deliver to</Text>
            <Text>{Address.addressDelivery}</Text>
            <Text style={{fontSize: 12}}>{Address.noteDelivery}</Text>
          </View>
        </View>
        <View>
          <Text style={styles.subBodyContent}>Services</Text>
          <View style={styles.contentServices}>
            <View
              style={{
                marginHorizontal: 10,
                flexDirection: 'column',
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  marginBottom: 5,
                  color: '#2B495D',
                }}>
                {DataToko.name}
              </Text>
              <Text> {DataToko.address.street}</Text>
            </View>
            <View>
              <Image
                style={{
                  width: 20,
                  height: 20,
                  marginRight: 20,
                  marginVertical: 5,
                }}
                source={ProfileNumber}
              />
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            padding: 10,
            paddingVertical: 15,
            borderColor: '#EFEFEF',
            borderWidth: 1,
            borderRadius: 10,
            marginHorizontal: 10,
          }}>
          <View>
            <Image
              style={{marginLeft: 10, width: 40, height: 40}}
              source={MainMenuWashAndIron}
            />
          </View>
          <View
            style={{
              flex: 1,
              marginLeft: 10,
              justifyContent: 'space-between',
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'column',
              }}>
              <Text
                style={{
                  ginBottom: 10,
                  color: '#2B495D',
                  fontWeight: 'bold',
                  marginBottom: 10,
                }}>
                {servicesName} {levelName}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Text>{typesName} :</Text>

                {DataLaundry.ItemClothes[0].id == '' ? null : (
                  <View>
                    {ItemClothes.map(e => {
                      return (
                        <View style={{flexDirection: 'row', marginLeft: 10}}>
                          <Text>{e.name}</Text>
                          <Text> </Text>
                          <Text>{e.quantity}</Text>
                        </View>
                      );
                    })}
                  </View>
                )}

                {DataLaundry.countWeight == 0 ? null : (
                  <View>
                    <Text>{DataLaundry.countWeight}kg</Text>
                  </View>
                )}
              </View>
            </View>
            <View>
              <Text style={{fontWeight: 'bold', color: '#2B495D'}}>
                Rp {PriceOrder}
              </Text>
            </View>
          </View>
        </View>
        <View>
          <Text
            style={{
              marginVertical: 10,
              marginHorizontal: 10,
              fontWeight: 'bold',
              fontSize: 18,
            }}>
            Payment
          </Text>

          <TouchableOpacity
            onPress={handleDropDownPayment}
            style={{
              marginHorizontal: 10,
              paddingHorizontal: 10,
              paddingVertical: 5,
              zIndex: 100,
              justifyContent: 'space-between',
              flexDirection: 'row',
              backgroundColor: '#EFEFEF',
            }}>
            <Text style={{marginVertical: 10}}>{paymentName}</Text>
            <AntDesign
              name={dDPayment === false ? 'down' : 'up'}
              size={20}
              color={'black'}
            />
          </TouchableOpacity>
          {dDPayment === false ? null : (
            <View>
              {payment.map(e => {
                console.log('PAYMENT', e);
                return (
                  <TouchableOpacity
                    style={{padding: 10, marginHorizontal: 10}}
                    // key={e.id}
                    onPress={() => handlePayment(e)}>
                    <Text>{e.value}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
          )}
          <View
            style={{
              backgroundColor: '#FFF0C5',
              padding: 10,
              marginVertical: 10,
              marginHorizontal: 10,
            }}>
            <Text style={{textAlign: 'justify'}}>
              The price may change if the weight of the clothes inputted does
              not match when it is calculated by the courier during pick up.
            </Text>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          justifyContent: 'flex-end',
          // backgroundColor: 'yellow',
        }}>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: '65%',
              paddingVertical: 10,
            }}>
            <Text
              style={{
                marginLeft: 10,
                paddingVertical: 10,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Total : {TotalPrice}
            </Text>
          </View>
          <TouchableOpacity
            onPress={async () => {
              await dispatch(PostCreateOrder(PaymentDetail));
              await dispatch(GetOnProcess());
              props.navigation.navigate('Order', {
                PricePayment: TotalPrice,
                DataDetailLaundry: DataLaundry,
                DataProfileToko: DataToko,
                PaymentDetail: Payment,
                orderPrice: PriceOrder,
                PriceDelivery: DeliveryFee,
                InputAddress: Address,
              });
            }}
            style={{width: '35%', backgroundColor: '#457899'}}>
            <Text
              style={{
                padding: 10,
                marginTop: 5,
                fontWeight: 'bold',
                alignSelf: 'center',
                color: 'white',
              }}>
              Order Now
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    backgroundColor: '#FF6C6C',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 20,
  },
  titlePage: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  subBodyContent: {
    marginVertical: 10,
    marginHorizontal: 10,
    fontWeight: 'bold',
    fontSize: 18,
  },
  textAddressInfo: {
    color: '#7A7A7A',
    textAlign: 'left',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  cardAddress: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    marginHorizontal: 10,
    width: '80%',
  },
  imageLoc: {
    width: 40,
    height: 40,
    marginHorizontal: 10,
  },
  imageDistanceLoc: {
    top: -20,
    marginBottom: -30,
    marginHorizontal: 30,
  },
  contentServices: {
    marginHorizontal: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 5,
    paddingVertical: 15,
    borderColor: '#EFEFEF',
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 10,
  },
});
