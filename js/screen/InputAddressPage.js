import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Touchable,
  Image,
  TextInput,
} from 'react-native';
import ProfilePickupDriver from '../assets/ProfilePickupDriver.png';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AddressNote from '../assets/AddressNote.png';
import {useDispatch} from 'react-redux';

const InputAddressPage = props => {
  console.log('ini data address', props);
  const [addressCustomer, setAddressCustomer] = useState('');
  const [addressDelivery, setAddressDelivery] = useState('');
  const [notePickUp, setNotePickUp] = useState('');
  const [noteDelivery, setNoteDelivery] = useState('');
  console.log(addressDelivery, noteDelivery);

  const DataProfile = props.route.params.DataProfileLaundry;
  const DataDetailLaundry = props.route.params.DataLaundry;
  console.log('data', DataDetailLaundry);
  const HandleAddress = {
    addressCustomer,
    addressDelivery,
    notePickUp,
    noteDelivery,
  };

  const [checkButton, setcheckButton] = useState(false);
  const handlecheckButton = item => {
    setcheckButton(!checkButton);
    setAddressDelivery(addressCustomer);
    setNoteDelivery(notePickUp);
  };

  //Edit Note
  const [editNote, setEditNote] = useState(false);
  const [editNote2, setEditNote2] = useState(false);
  const handleEditNote = () => {
    setEditNote(!editNote);
  };
  const handleEditNote2 = () => {
    setEditNote2(!editNote2);
  };

  //Price
  const [deliveryFee, setDeliveryFee] = useState(12000);
  const DeliveryFee = deliveryFee;
  const Total = props.route.params.Total;
  const Price = Total + DeliveryFee;

  return (
    <View style={styles.container}>
      {/* header */}
      <View style={styles.header}>
        <AntDesign
          name="left"
          size={20}
          color={'white'}
          onPress={() => props.navigation.goBack()}
        />
        <Text style={styles.titleHeader}>Address</Text>
        <Text></Text>
      </View>

      {/* body */}
      <View style={styles.bodyDeliveryFee}>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.imagePickUp} source={ProfilePickupDriver} />
          <Text style={{color: '#2B495D', fontWeight: 'bold'}}>
            Delivery Fee
          </Text>
        </View>
        <Text style={{color: '#2B495D', fontWeight: 'bold'}}>Rp 12000</Text>
      </View>

      {/* Pick Up */}
      <View>
        <Text style={styles.bodyContent}>Pick up at</Text>
        <TextInput
          style={styles.inputAddress}
          placeholder="type your address"
          placeholderTextColor="#D6D6D6"
          value={addressCustomer}
          onChangeText={setAddressCustomer}
        />
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 10,
          }}>
          <TouchableOpacity
            onPress={() => {
              handleEditNote();
            }}>
            <Text style={styles.textNote}>
              <Image style={styles.imageNote} source={AddressNote} />
              Add Note
            </Text>
          </TouchableOpacity>
          {editNote === true ? (
            <TextInput
              style={styles.inputEditNote}
              multiline
              value={notePickUp}
              onChangeText={setNotePickUp}
            />
          ) : null}
        </View>
      </View>

      {/* Delivery to */}
      <View>
        <Text style={styles.bodyContent}>Deliver to</Text>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
            marginHorizontal: 10,
          }}>
          {checkButton === false ? (
            <TouchableOpacity
              style={styles.checkButton}
              onPress={handlecheckButton}>
              <MaterialIcons name="crop-square" size={30} color={'black'} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.checkButton}
              onPress={handlecheckButton}>
              <AntDesign
                name="checksquareo"
                size={30}
                color={'black'}
                style={{backgroundColor: 'white'}}
              />
            </TouchableOpacity>
          )}
          <Text
            style={{
              color: 'black',
              marginBottom: 10,
              marginTop: 7,
            }}>
            Same with Pick Up Address
          </Text>
        </View>

        {checkButton == false ? (
          <View>
            <TextInput
              style={styles.inputAddress}
              placeholder="type your address"
              placeholderTextColor="#D6D6D6"
              value={addressDelivery}
              onChangeText={setAddressDelivery}
            />
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 10,
              }}>
              <TouchableOpacity
                onPress={() => {
                  handleEditNote2();
                }}>
                <Text style={styles.textNote}>
                  <Image style={styles.imageNote} source={AddressNote} />
                  Add Note
                </Text>
              </TouchableOpacity>
              {editNote2 === true ? (
                <TextInput
                  style={styles.inputEditNote}
                  multiline
                  value={noteDelivery}
                  onChangeText={setNoteDelivery}
                />
              ) : null}
            </View>
          </View>
        ) : null}
      </View>

      {/* Footer */}
      <View style={styles.footer}>
        <View
          style={{
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <View style={styles.footerPrice}>
            <Text
              style={{
                padding: 5,
                marginHorizontal: 10,
                alignSelf: 'stretch',
                fontWeight: 'bold',
              }}>
              Total
            </Text>
            <Text
              style={{
                padding: 5,
                marginHorizontal: 10,
                alignSelf: 'stretch',
              }}>
              Price : {Total} + {DeliveryFee}
            </Text>
          </View>

          {(addressCustomer.length > 5 && checkButton == true) ||
          addressDelivery.length > 5 ? (
            <TouchableOpacity
              onPress={() =>
                props.navigation.navigate('PaymentPage', {
                  Address: HandleAddress,
                  PriceFinal: Price,
                  PriceOrder: Total,
                  DataOrder: DataDetailLaundry,
                  DataDetailToko: DataProfile,
                })
              }
              style={{width: '35%', backgroundColor: '#457899'}}>
              <Text style={{padding: 10, textAlign: 'left', color: 'white'}}>
                Next
              </Text>
              <Text
                style={{
                  marginHorizontal: 10,
                  textAlign: 'left',
                  color: 'white',
                }}>
                Select Payment
              </Text>
            </TouchableOpacity>
          ) : (
            <View style={styles.footerNext2}>
              <Text style={{padding: 10, textAlign: 'left', color: 'black'}}>
                Next
              </Text>
              <Text style={styles.NextInputAddress2}>Select Payment</Text>
            </View>
          )}
        </View>
      </View>
    </View>
  );
};

export default InputAddressPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    zIndex: -1,
  },
  header: {
    backgroundColor: '#FF6C6C',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 20,
  },
  titleHeader: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  bodyDeliveryFee: {
    backgroundColor: '#FFF0C5',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 10,
  },
  bodyContent: {
    marginVertical: 10,
    marginHorizontal: 10,
    fontWeight: 'bold',
    fontSize: 18,
  },
  services: {
    borderColor: '#457899',
    borderWidth: 1,
    borderRadius: 20,
    marginRight: 10,
    marginBottom: 10,
  },
  types: {
    borderColor: '#457899',
    borderWidth: 1,
    borderRadius: 20,
    marginRight: 10,
    marginBottom: 10,
  },
  imagePickUp: {
    width: 12,
    height: 12,
    marginTop: 5,
    marginBottom: 5,
    marginRight: 10,
  },
  inputAddress: {
    borderRadius: 7,
    backgroundColor: '#ECECEC',
    borderColor: '#ECECEC',
    borderWidth: 2,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  textNote: {
    borderColor: '#457899',
    borderWidth: 1,
    padding: 10,
    borderRadius: 20,
    marginRight: 10,
    marginVertical: 10,
  },
  imageNote: {
    width: 12,
    height: 12,
    marginTop: 5,
    marginBottom: 5,
    marginRight: 10,
    padding: 10,
  },
  footer: {
    flex: 1,

    justifyContent: 'flex-end',
  },
  footerPrice: {
    width: '65%',
    alignItems: 'center',
  },
  inputEditNote: {
    backgroundColor: 'white',
    padding: 10,
    width: '64%',
    borderRadius: 10,
    borderColor: '#2B495D',
    borderWidth: 1,
    marginHorizontal: 20,
    marginTop: 10,
    zIndex: 100,
    marginBottom: 20,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  footerNext2: {
    width: '35%',
    backgroundColor: '#EFEFEF',
  },

  NextInputAddress2: {
    marginHorizontal: 10,
    textAlign: 'left',
    color: 'black',
  },
});
