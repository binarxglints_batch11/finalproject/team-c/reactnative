import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {Image} from 'react-native';
import homePage from './homePage';
import accountPage from './accountPage';
import listPage from './listPage';
import orderPage from './orderPage';
import MainTabHome from '../assets/MainTabHome.png';
import MainTabList from '../assets/MainTabList.png';
import MainTabOrder from '../assets/MainTabOrder.png';
import MainTabAccount from '../assets/MainTabAccount.png';

const Tab = createMaterialBottomTabNavigator();

const MainTab = ({navigation}) => (
  <Tab.Navigator
    initialRouteName="Home"
    activeColor="#FF415B"
    shifting={false}
    barStyle={{
      backgroundColor: 'white',
    }}>
    <Tab.Screen
      name="Home"
      component={homePage}
      options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({color}) => (
          <Image style={{tintColor: color}} source={MainTabHome} />
        ),
      }}
    />

    <Tab.Screen
      name="Order"
      component={orderPage}
      options={{
        tabBarLabel: 'Order',
        tabBarIcon: ({color}) => (
          <Image style={{tintColor: color}} source={MainTabOrder} />
        ),
      }}
    />
    <Tab.Screen
      name="Profile"
      component={accountPage}
      options={{
        tabBarLabel: 'Profile',
        tabBarIcon: ({color}) => (
          <Image style={{tintColor: color}} source={MainTabAccount} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTab;
