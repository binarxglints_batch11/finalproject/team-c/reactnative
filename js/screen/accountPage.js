import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BGHome from '../assets/BGHome.png';
import SearchBar from '../screen/Component/Search/index';
import AccountPhoto from '../assets/AccountPhoto.png';
import AccountSetting from '../assets/AccountSetting.png';
import AccountContactUs from '../assets/AccountContactUs.png';
import AccountSavedLaundry from '../assets/AccountSavedLaundry.png';
import AccountFavoriteServices from '../assets/AccountFavoriteServices.png';
import AccountAboutUs from '../assets/AccountAboutUs.png';
import AccountTermsConditions from '../assets/AccountTermsConditions.png';
import AccountLogout from '../assets/AccountLogout.png';
import ProfileEdit from '../assets/ProfileEdit.png';
import save from '../assets/save.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';
import {GetEditProfile, GetProfile} from '../Redux/action/ActionMenu';

const accountPage = ({navigation}) => {
  const {profile} = useSelector(state => state.AllLaundryReducers);
  const dispatch = useDispatch();
  const [name, setName] = useState(profile.name);
  const [mobile_phone, setMobile_phone] = useState(profile.mobile_phone);

  const [editProfile, setEditProfile] = useState(false);

  useEffect(() => {
    dispatch(GetProfile());
  }, []);

  const removeValue = async () => {
    try {
      await AsyncStorage.removeItem('token');
      navigation.navigate('loginPage');
    } catch (e) {}

    console.log('Done.');
  };
  //cancel
  const handleEditProfile = () => {
    setEditProfile(!editProfile);
  };

  //ok
  const editProfileHandler = async () => {
    await dispatch(GetEditProfile(name, mobile_phone));
    await dispatch(GetProfile());
    handleEditProfile();
  };

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGHome}>
        {/* header */}
        <View style={styles.header}>
          <SearchBar />
          <View style={{marginLeft: 10}}>
            <TouchableOpacity>
              <AntDesign name="bells" size={25} color={'white'} />
            </TouchableOpacity>
          </View>
        </View>
        {/* body */}
        <View style={styles.cardProfile}>
          <View
            style={{
              width: '20%',
              marginVertical: 20,
              marginHorizontal: 20,
            }}>
            <Image style={styles.accountPhoto} source={AccountPhoto} />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={{justifyContent: 'center'}}>
              <Text style={{fontWeight: 'bold', marginBottom: 5}}>
                {profile.name}
              </Text>

              <Text styles={{marginBottom: 5}}>{profile.email}</Text>
            </View>
          </View>
          {editProfile == true ? (
            <TouchableOpacity
              onPress={() => {
                editProfileHandler();
              }}>
              <Image
                style={{
                  marginVertical: 10,
                  marginLeft: 10,
                  width: 20,
                  height: 20,
                }}
                source={save}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                handleEditProfile();
              }}>
              <Image
                style={{
                  marginVertical: 10,
                  marginLeft: 10,
                  width: 20,
                  height: 20,
                }}
                source={ProfileEdit}
              />
            </TouchableOpacity>
          )}
        </View>

        {editProfile === true && (
          <View style={styles.editProfileMenu}>
            <Text style={{fontWeight: 'bold'}}>Edit Profile :</Text>

            <TextInput
              style={styles.inputEditProfile}
              onChangeText={setName}
              value={name}
              placeholderTextColor="black"
            />
            <TextInput
              style={styles.inputEditProfile}
              onChangeText={setMobile_phone}
              value={mobile_phone}
              placeholder="Mobile Phone"
              placeholderTextColor="black"
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                marginTop: 20,
              }}>
              {/* <TouchableOpacity
                style={{marginRight: 20, backgroundColor: 'yellow'}}
                onPress={() => {
                  // editProfileHandler;
                  setEditProfile(false);
                }}>
                <Text>OK</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{backgroundColor: 'pink'}}
                onPress={() => {
                  console.log('heloo');
                  // handleEditProfile;
                  setEditProfile(false);
                }}>
                <Text>Cancel</Text>
              </TouchableOpacity> */}
            </View>
          </View>
        )}
      </ImageBackground>

      <View
        style={{
          borderRadius: 10,
          marginTop: -5,
          backgroundColor: 'white',
          marginVertical: 20,
          marginHorizontal: 20,
          zIndex: -1,
        }}>
        <TouchableOpacity style={styles.accountBox}>
          <Image source={AccountSetting} />
          <Text style={styles.textAccountBox}>Account Setting</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.accountBox}>
          <Image source={AccountContactUs} />
          <Text style={styles.textAccountBox}>Contact Us</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.accountBox}>
          <Image source={AccountSavedLaundry} />
          <Text style={styles.textAccountBox}>Saved Laundry</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.accountBox}>
          <Image source={AccountFavoriteServices} />
          <Text style={styles.textAccountBox}>Favorite Service</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.accountBox}>
          <Image source={AccountAboutUs} />
          <Text style={styles.textAccountBox}>About Us</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.accountBox}>
          <Image source={AccountTermsConditions} />
          <Text style={styles.textAccountBox}>Terms & Conditions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.accountBox} onPress={removeValue}>
          <Image source={AccountLogout} />
          <Text style={styles.textAccountBox}>Logout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    marginTop: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  search: {
    width: '80%',
    height: 36,
    marginTop: 7,
    backgroundColor: 'white',
    color: 'black',
    borderRadius: 10,
  },
  logo: {
    alignContent: 'center',
    height: 240,
  },
  logoNotif: {
    width: '20%',
    flexDirection: 'row',
    height: 36,
  },
  editProfileMenu: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    marginHorizontal: 20,
    zIndex: 999,
    marginBottom: 20,
    marginTop: -80,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  cardProfile: {
    backgroundColor: 'white',
    flexDirection: 'row',
    borderColor: '#EFEFEF',
    borderWidth: 1,
    borderRadius: 10,
    marginTop: 30,
    marginBottom: 10,
    marginHorizontal: 20,
  },
  accountBox: {
    zIndex: -1,
    height: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#EFEFEF',
    padding: 10,
    marginRight: 10,
    flexDirection: 'row',
  },
  textAccountBox: {
    color: 'gray',
    marginLeft: 10,
  },
  accountPhoto: {
    marginHorizontal: 10,
    marginVertical: 10,
    width: 40,
    height: 40,
  },
  inputEditProfile: {
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
    width: 280,
  },
});

export default accountPage;
