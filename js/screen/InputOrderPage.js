import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useDispatch} from 'react-redux';
import {PostAddToList} from '../Redux/action/ActionMenu';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

const InputOrderPage = props => {
  console.log('input order', props);
  const dispatch = useDispatch();

  const dataDetail = props.route.params.detail;

  const ItemsDropDown = props.route.params.detail.basic.byItem.item;
  const ItemsDropDownWeight = props.route.params.detail.basic.byItem.item;

  const idLaundry = props.route.params.a.id;
  const ProfileLaundry = props.route.params.a;
  const basicWeightPrice = dataDetail.basic.byWeight.price;
  const expressWeightPrice = dataDetail.express.byWeight.price;

  const [dDServices, setdDServices] = useState(false);
  const [dDTypes, setdDTypes] = useState(false);
  const [dDClothes, setdDClothes] = useState(false);
  const [dDClothesWeight, setDDClothesWeight] = useState(false);
  const [dDClothesAdd, setDDClothesAdd] = useState(false);
  const [dDclothesAdd1, setDDclothesAdd1] = useState(false);

  const [servicesName, setServicesName] = useState(
    dataDetail.name + ' ' + dataDetail.basic.name,
  );

  const handleDropDownServices = () => {
    setdDServices(!dDServices);
  };
  const handleDropDownTypes = () => {
    setdDTypes(!dDTypes);
  };

  //item
  const handleDropDownClothes = () => {
    setdDClothes(!dDClothes);
  };
  //weight
  const handleDropDownClothesWeight = () => {
    setDDClothesWeight(!dDClothesWeight);
  };

  const handleDropDownClothesAdd = () => {
    setDDClothesAdd(!dDClothesAdd);
  };
  const handleDropDownClothesAdd1 = () => {
    setDDclothesAdd1(!dDclothesAdd1);
  };

  //SERVICES
  const services1 = [
    {id: '2', value: dataDetail?.name + ' ' + dataDetail?.express?.name},
  ];
  const services2 = [
    {id: '1', value: dataDetail?.name + ' ' + dataDetail?.basic.name},
  ];
  const [levelServices, setLevelServices] = useState(1);

  const servicesId = dataDetail.id;

  const handleServices = value => {
    setServicesName(value.value);
    setdDServices(!dDServices);
    setLevelServices(value.id);
  };

  //TYPES
  const types = [
    {id: 1, value: 'by Item'},
    {id: 2, value: 'by Kilogram'},
  ];
  const [typesId, setTypesId] = useState('');
  const [typesName, setTypesName] = useState('Select Types');

  const handleTypes = value => {
    setTypesName(value.value);
    setTypesId(value.id);
    setCountWeight(0);
    setcheckButton(false);
    setCount(0);
    setCount1(0);
    setCount2(0);
    setdDTypes(!dDTypes);
  };

  //CLOTHES DETAIL
  //name
  const [clothesName, setClothesName] = useState('Select item');
  const [clothesNameWeight, setClothesNameWeight] = useState('Select item');
  const [clothesAdd, setClothesAdd] = useState('Select item');
  const [clothesAdd1, setClothesAdd1] = useState('Select item');
  const [clothesId, setClothesId] = useState('');
  const [clothesIdWeight, setClothesIdWeight] = useState('');
  const [clothesIdAdd, setClothesIdAdd] = useState('');
  const [clothesIdAdd1, setClothesIdAdd1] = useState('');

  //price
  const [clothesPrice, setClothesPrice] = useState(0);
  const [clothesPriceAdd, setClothesPriceAdd] = useState(0);
  const [clothesPriceAdd1, setClothesPriceAdd1] = useState(0);

  const clothesPriceWeight =
    servicesName === dataDetail.name + ' ' + dataDetail.basic.name
      ? basicWeightPrice
      : expressWeightPrice;

  //count
  const [count, setCount] = useState(0);
  const [count1, setCount1] = useState(0);
  const [count2, setCount2] = useState(0);
  const [countWeight, setCountWeight] = useState(0);
  const [countDetailWeight, setCountDetailWeight] = useState(0);

  //tagAdd
  const [tagAdd, setTagAdd] = useState(0);

  const [tagAddWeight, setTagAddWeight] = useState(0);

  //count -/+
  const handleCountMinus = () => {
    if (count > 0) {
      setCount(count - 1);
    }
  };
  const handleCountPlus = () => {
    setCount(count + 1);
    setcheckButton(true);
  };
  const handleCountMinus1 = () => {
    if (count1 > 0) {
      setCount1(count1 - 1);
    }
  };
  const handleCountPlus1 = () => {
    setCount1(count1 + 1);
  };
  const handleCountMinus2 = () => {
    if (count2 > 0) {
      setCount2(count2 - 1);
    }
  };
  const handleCountPlus2 = () => {
    setCount2(count2 + 1);
  };

  //byWeight
  const handleCountMinusWeight = () => {
    if (countWeight > 0) {
      setCountWeight(countWeight - 1);
    }
  };
  const handleCountPlusWeight = () => {
    setCountWeight(countWeight + 1);
  };
  const handleCountMinusDetailWeight = () => {
    if (countDetailWeight > 0) {
      setCountDetailWeight(countDetailWeight - 1);
    }
  };
  const handleCountPlusDetailWeight = () => {
    setCountDetailWeight(countDetailWeight + 1);
  };

  //add
  const handleTagAdd = () => {
    setTagAdd(tagAdd + 1);
  };
  const handleTagAddWeight = () => {
    setTagAddWeight(tagAddWeight + 1);
  };
  const handleTagRemove = () => {
    setTagAdd(tagAdd - 1);
  };

  const handleClothes = value => {
    setClothesName(value.name);
    setClothesPrice(value.price);
    setClothesId(value.id);
    setdDClothes(!dDClothes);
  };

  const handleClothesWeight = value => {
    setClothesNameWeight(value.name);
    setClothesIdWeight(value.id);
    setDDClothesWeight(!dDClothesWeight);
  };

  const handleClothesAdd = value => {
    setClothesAdd(value.name);
    setClothesPriceAdd(value.price);
    setClothesIdAdd(value.id);
    setDDClothesAdd(!dDClothesAdd);
  };
  const handleClothesAdd1 = value => {
    setClothesAdd1(value.name);
    setClothesPriceAdd1(value.price);
    setClothesIdAdd1(value.id);
    setDDclothesAdd1(!dDclothesAdd1);
  };

  const PriceClothes = clothesPrice * count;
  const PriceClothes1 = clothesPriceAdd * count1;
  const PriceClothes2 = clothesPriceAdd1 * count2;
  const PriceClothesWeight = clothesPriceWeight * countWeight;

  const TotalPrice =
    PriceClothes + PriceClothes1 + PriceClothes2 + PriceClothesWeight;

  const [checkButton, setcheckButton] = useState(false);
  const handlecheckButton = item => {
    setcheckButton(!checkButton);
  };

  //items clothes

  const itemsClothes1 = [
    {
      id: clothesId,
      name: clothesName,
      quantity: count,
    },
  ];
  const itemsClothesWeight = [
    {
      id: clothesIdWeight,
      name: clothesNameWeight,
      quantity: countDetailWeight,
    },
  ];
  const itemsClothes2 = [
    {
      id: clothesId,
      name: clothesName,
      quantity: count,
    },
    {
      id: clothesIdAdd,
      name: clothesAdd,
      quantity: count1,
    },
  ];
  const itemsClothes3 = [
    {
      id: clothesId,
      name: clothesName,
      quantity: count,
    },
    {
      id: clothesIdAdd,
      name: clothesAdd,
      quantity: count1,
    },
    {
      id: clothesIdAdd1,
      name: clothesAdd1,
      quantity: count2,
    },
  ];

  const ItemClothes =
    tagAdd == 0 ? itemsClothes1 : tagAdd == 1 ? itemsClothes2 : itemsClothes3;
  console.log('item clothes', ItemClothes);

  const DataOrderLaundry = {
    typesId,
    servicesId,
    levelServices,
    countWeight,
    ItemClothes,
  };

  const PostAddToList = async (
    idLaundry,
    typesId,
    servicesId,
    levelServices,
    countWeight,
    ItemClothes,
    TotalPrice,
  ) => {
    const token = await AsyncStorage.getItem('token');

    var FormData = require('form-data');
    var data = new FormData();
    data.append('laundry', idLaundry);
    data.append('type', typesId);
    data.append('service', servicesId);
    data.append('level', levelServices);
    data.append('weight', countWeight);
    data.append('item', ItemClothes);
    data.append('price', TotalPrice);

    var config = {
      method: 'POST',
      url: 'https://washme.gabatch11.my.id/list/add',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log('responAddToList', response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <AntDesign
          name="left"
          size={20}
          color={'white'}
          onPress={() => props.navigation.goBack()}
        />
        <Text style={styles.titlePage}>Order</Text>
        <TouchableOpacity onPress={() => PostAddToList()}>
          <Text style={styles.addToList}>Add to List</Text>
        </TouchableOpacity>
      </View>

      {/* Services */}
      <View style={{flex: 1}}>
        <View>
          <Text style={styles.subTitle}>Services</Text>
          <TouchableOpacity
            onPress={handleDropDownServices}
            style={styles.DropDown}>
            <Text style={{marginVertical: 10}}>{servicesName}</Text>
            <AntDesign
              name={dDServices === false ? 'down' : 'up'}
              size={20}
              color={'black'}
            />
          </TouchableOpacity>

          {dDServices === false ? null : (
            <View>
              {servicesName == dataDetail.name + ' ' + dataDetail.basic.name
                ? services1.map(e => {
                    return (
                      <TouchableOpacity
                        style={{
                          padding: 10,
                          marginHorizontal: 10,
                          borderColor: '#EFEFEF',
                          borderWidth: 1,
                        }}
                        key={e.id}
                        onPress={() => handleServices(e)}>
                        <Text>
                          {dataDetail.express == undefined ? null : e.value}
                        </Text>
                      </TouchableOpacity>
                    );
                  })
                : services2.map(e => {
                    return (
                      <TouchableOpacity
                        style={{
                          padding: 10,
                          marginHorizontal: 10,
                          borderColor: '#EFEFEF',
                          borderWidth: 1,
                        }}
                        key={e.id}
                        onPress={() => handleServices(e)}>
                        <Text>{e.value}</Text>
                      </TouchableOpacity>
                    );
                  })}
            </View>
          )}
        </View>

        {/* Types */}
        <View>
          <Text style={styles.subTitle}>Types</Text>
          <TouchableOpacity
            onPress={handleDropDownTypes}
            style={styles.DropDown}>
            <Text style={{marginVertical: 10}}>{typesName}</Text>

            <AntDesign
              name={dDTypes === false ? 'down' : 'up'}
              size={20}
              color={'black'}
            />
          </TouchableOpacity>

          {/* Types condition (item/kg) */}
          {dDTypes === false ? null : (
            <View>
              {types.map(e => {
                return (
                  <TouchableOpacity
                    style={{padding: 10, marginHorizontal: 10}}
                    key={e.id}
                    onPress={() => handleTypes(e)}>
                    <Text>{e.value}</Text>
                  </TouchableOpacity>
                );
              })}
            </View>
          )}
        </View>

        {/* byKilogram */}

        {typesName === 'by Kilogram' ? (
          <ScrollView>
            <Text style={styles.subTitle}>Weight</Text>
            <View style={{flexDirection: 'row', marginBottom: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  flex: 1,
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={handleCountMinusWeight}
                  style={styles.handleCount}>
                  <Text style={{fontSize: 30}}>-</Text>
                </TouchableOpacity>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    adding: 10,
                  }}>
                  <Text style={{textAlign: 'center'}}>{countWeight}</Text>
                </View>
                <TouchableOpacity
                  onPress={handleCountPlusWeight}
                  style={styles.handleCount}>
                  <Text style={{fontSize: 30}}>+</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <Text style={styles.textAlert}>
                The price may change if the weight of the clothes inputted does
                not match when it is calculated by the courier during pick up.
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginVertical: 10,
                  marginHorizontal: 10,
                }}>
                {checkButton === false ? (
                  <TouchableOpacity
                    style={styles.checkButton}
                    onPress={handlecheckButton}>
                    <MaterialIcons
                      name="crop-square"
                      size={30}
                      color={'black'}
                    />
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    style={styles.checkButton}
                    onPress={handlecheckButton}>
                    <AntDesign
                      name="checksquareo"
                      size={30}
                      color={'black'}
                      style={{backgroundColor: 'white'}}
                    />
                  </TouchableOpacity>
                )}
                <Text
                  style={{
                    color: 'black',
                    marginBottom: 10,
                    marginTop: 7,
                  }}>
                  I Understand
                </Text>
              </View>
            </View>
          </ScrollView>
        ) : (
          // BY ITEM
          <View>
            <Text style={styles.subTitle}>Clothes Detail</Text>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={handleDropDownClothes}
                style={styles.DropDownClothesDetail}>
                <Text style={{padding: 10, marginHorizontal: 10}}>
                  {clothesName}
                </Text>
                <AntDesign
                  name={dDClothes === false ? 'down' : 'up'}
                  size={20}
                  color={'black'}
                  style={{padding: 10}}
                />
              </TouchableOpacity>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',

                  flex: 1,
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={handleCountMinus}
                  style={styles.handleCount}>
                  <Text style={{fontSize: 30}}>-</Text>
                </TouchableOpacity>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                  }}>
                  <Text style={{textAlign: 'center'}}>{count}</Text>
                </View>
                <TouchableOpacity
                  onPress={handleCountPlus}
                  style={styles.handleCount}>
                  <Text style={{fontSize: 30}}>+</Text>
                </TouchableOpacity>
              </View>
            </View>
            {dDClothes === false ? null : (
              <View>
                {ItemsDropDown.map(e => {
                  console.log(e);
                  return (
                    <TouchableOpacity
                      style={{padding: 10, marginHorizontal: 10}}
                      key={e.id}
                      onPress={() => handleClothes(e)}>
                      <Text>{e.name}</Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            )}

            {/* ADD ITEM LIST */}

            {/* add list 1 */}
            {tagAdd > 0 && (
              <View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <TouchableOpacity
                    onPress={handleDropDownClothesAdd}
                    style={styles.DropDownClothesDetail}>
                    <Text style={{padding: 10, marginHorizontal: 10}}>
                      {clothesAdd}
                    </Text>
                    <AntDesign
                      name={dDClothesAdd === false ? 'down' : 'up'}
                      size={20}
                      color={'black'}
                      style={{padding: 10}}
                    />
                  </TouchableOpacity>

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',

                      flex: 1,
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={handleCountMinus1}
                      style={styles.handleCount}>
                      <Text style={{fontSize: 30}}>-</Text>
                    </TouchableOpacity>
                    <View
                      style={{
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <Text style={{textAlign: 'center'}}>{count1}</Text>
                    </View>
                    <TouchableOpacity
                      onPress={handleCountPlus1}
                      style={styles.handleCount}>
                      <Text style={{fontSize: 30}}>+</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                {dDClothesAdd === false ? null : (
                  <View>
                    {ItemsDropDown.map(e => {
                      console.log(e);
                      return (
                        <TouchableOpacity
                          style={{padding: 10, marginHorizontal: 10}}
                          key={e.id}
                          onPress={() => handleClothesAdd(e)}>
                          <Text>{e.name}</Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                )}
              </View>
            )}

            {/* add list 2 */}
            {tagAdd > 1 && (
              <View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <TouchableOpacity
                    onPress={handleDropDownClothesAdd1}
                    style={styles.DropDownClothesDetail}>
                    <Text style={{padding: 10, marginHorizontal: 10}}>
                      {clothesAdd1}
                    </Text>
                    <AntDesign
                      name={dDclothesAdd1 === false ? 'down' : 'up'}
                      size={20}
                      color={'black'}
                      style={{padding: 10}}
                    />
                  </TouchableOpacity>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',

                      flex: 1,
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={handleCountMinus2}
                      style={styles.handleCount}>
                      <Text style={{fontSize: 30}}>-</Text>
                    </TouchableOpacity>
                    <View
                      style={{
                        flex: 1,
                        alignItems: 'center',
                      }}>
                      <Text style={{textAlign: 'center'}}>{count2}</Text>
                    </View>
                    <TouchableOpacity
                      onPress={handleCountPlus2}
                      style={styles.handleCount}>
                      <Text style={{fontSize: 30}}>+</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                {dDclothesAdd1 === false ? null : (
                  <View>
                    {ItemsDropDown.map(e => {
                      console.log(e);
                      return (
                        <TouchableOpacity
                          style={{padding: 10, marginHorizontal: 10}}
                          key={e.id}
                          onPress={() => handleClothesAdd1(e)}>
                          <Text>{e.name}</Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                )}
              </View>
            )}

            <View style={{flexDirection: 'row'}}>
              {/* Add Button */}
              {tagAdd < 2 && (
                <TouchableOpacity
                  onPress={handleTagAdd}
                  style={styles.tagAddButton}>
                  <Text style={{}}> + Add</Text>
                </TouchableOpacity>
              )}
              {/* Remove Button */}
              {tagAdd > 0 && (
                <TouchableOpacity
                  onPress={handleTagRemove}
                  style={styles.tagAddButton}>
                  <Text style={{}}> - Remove</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        )}
      </View>
      {/* footer */}
      <View>
        <View style={styles.footer}>
          <View
            style={{
              justifyContent: 'space-between',
              flexDirection: 'row',
            }}>
            <View style={styles.footerPrice}>
              <Text
                style={{
                  padding: 5,
                  marginHorizontal: 10,
                  alignSelf: 'stretch',
                }}>
                Subtotal ({count + count1 + count2 + countWeight})
              </Text>
              <Text
                style={{
                  padding: 5,
                  marginHorizontal: 10,
                  alignSelf: 'stretch',
                }}>
                Price : Rp {TotalPrice}
              </Text>
            </View>

            {checkButton === true ? (
              <TouchableOpacity
                onPress={() =>
                  props.navigation.navigate('InputAddressPage', {
                    Total: TotalPrice,
                    DataLaundry: DataOrderLaundry,
                    DataProfileLaundry: ProfileLaundry,
                  })
                }
                style={styles.footerNext}>
                <Text style={{padding: 10, textAlign: 'left', color: 'white'}}>
                  Next
                </Text>
                <Text style={styles.NextInputAddress}>Input Address</Text>
              </TouchableOpacity>
            ) : (
              <View style={styles.footerNext2}>
                <Text style={{padding: 10, textAlign: 'left', color: 'black'}}>
                  Next
                </Text>
                <Text style={styles.NextInputAddress2}>Input Address</Text>
              </View>
            )}
          </View>
        </View>
      </View>
    </View>
  );
};

export default InputOrderPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    backgroundColor: '#FF6C6C',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 20,
  },
  titlePage: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  addToList: {
    color: 'white',
    fontWeight: 'bold',
  },
  subTitle: {
    padding: 10,
    fontWeight: 'bold',
    fontSize: 18,
    marginHorizontal: 10,
  },
  DropDown: {
    alignItems: 'center',
    marginHorizontal: 10,
    padding: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#EFEFEF',
  },
  DropDownClothesDetail: {
    width: '60%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginHorizontal: 10,
    backgroundColor: '#EFEFEF',
  },
  informationPriceTypes: {
    marginHorizontal: 10,
    marginVertical: 5,
    color: '#898989',
  },
  handleCount: {
    marginHorizontal: 10,
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FF415B',
  },
  tagAddButton: {
    width: '25%',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 20,
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 10,
  },
  footer: {
    justifyContent: 'flex-end',
  },
  footerPrice: {
    width: '65%',
    alignItems: 'center',
  },
  footerNext: {
    width: '35%',
    backgroundColor: '#457899',
  },
  footerNext2: {
    width: '35%',
    backgroundColor: '#EFEFEF',
  },
  NextInputAddress: {
    marginHorizontal: 10,
    textAlign: 'left',
    color: 'white',
  },
  NextInputAddress2: {
    marginHorizontal: 10,
    textAlign: 'left',
    color: 'black',
  },
  textAlert: {
    backgroundColor: '#FFF0C5',
    marginHorizontal: 10,
    marginVertical: 10,
    textAlign: 'justify',
    padding: 10,
  },
});
