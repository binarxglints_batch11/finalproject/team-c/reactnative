import React, {useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  StatusBar,
  ImageBackground,
} from 'react-native';
import logoGetStarted from '../../assets/logoGetStarted.png';
import Button from '../Component/Button/index';

const landingGetStarted = ({navigation}) => {
  return (
    <View style={style.container}>
      <ImageBackground style={style.logo} source={logoGetStarted} />
      <Text style={style.textBranding}>
        Easy peasy to get your ideal laundry {'\n'} services anywhere and
        anytime
      </Text>
      <View
        style={{
          width: '90%',
          height: '20%',
          justifyContent: 'center',
        }}>
        <Button
          textButton="Get Started"
          onPressButton={() => navigation.navigate('registerPage')}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  logo: {
    width: 250,
    height: 250,
    aspectRatio: 1,
    marginBottom: 30,
    marginLeft: 20,
  },
  textBranding: {
    color: '#000',
    fontSize: 16,
    marginBottom: 20,
    marginTop: 15,
    textAlign: 'center',
  },
});
export default landingGetStarted;
