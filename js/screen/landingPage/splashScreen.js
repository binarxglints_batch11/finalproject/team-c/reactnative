import React, {useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  StatusBar,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import logoSplashScreen from '../../assets/logoSplashScreen.png';
import {useDispatch} from 'react-redux';
import {GetProfile} from '../../Redux/action/ActionMenu';
import {getNearYou} from '../../Redux/action';

const splashScreen = ({navigation}) => {
  const dispatch = useDispatch();

  useEffect(() => {
    AsyncStorage.getItem('token', (err, res) => {
      if (res) {
        dispatch(GetProfile());
        dispatch(getNearYou());
        setTimeout(async () => {
          await navigation.navigate('MainTab');
        }, 2000);
      } else {
        setTimeout(() => {
          navigation.replace('landingGetStarted');
        }, 3000);
      }
    });
  }, []);

  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={logoSplashScreen} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FF6C6C',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 250,
    height: 270,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default splashScreen;
