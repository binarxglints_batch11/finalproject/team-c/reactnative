import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Text,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import BGHome from '../assets/BGHome.png';
import HomePageWash from '../assets/HomePageWash.png';
import HomePageWashAndIron from '../assets/HomePageWashAndIron.png';
import HomePageIron from '../assets/HomePageIron.png';
import HomePageDryClean from '../assets/HomePageDryClean.png';
import HomePageShoes from '../assets/HomePageShoes.png';
import HomePageHousehold from '../assets/HomePageHousehold.png';
import CardNearYou from '../screen/Component/CardNearYou/index';
import SearchBar from '../screen/Component/Search/index';
import CardNeedHelp from './Component/NeedHelp';
import {getNearYou} from '../Redux/action/index';
import {GetMenu, GetProfile} from '../Redux/action/ActionMenu';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const homePage = ({navigation}) => {
  const {AllLaundry, profile} = useSelector(state => state.AllLaundryReducers);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getNearYou());
    dispatch(GetProfile());
  }, []);

  const [servicesMenu, setServicesMenu] = useState([
    {idService: 1, image: HomePageWashAndIron, servicesName: 'Wash And Iron'},
    {idService: 2, image: HomePageWash, servicesName: 'Wash'},
    {idService: 3, image: HomePageIron, servicesName: 'Iron'},
    {idService: 4, image: HomePageDryClean, servicesName: 'Dry Clean'},
    {idService: 5, image: HomePageShoes, servicesName: 'Shoes'},
    {idService: 6, image: HomePageHousehold, servicesName: 'Household'},
  ]);

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGHome} />

      {/* header */}
      <View style={styles.header}>
        <TouchableOpacity
          style={styles.search}
          onPress={() => navigation.navigate('SearchPage')}>
          <SearchBar />
        </TouchableOpacity>
        <View style={{marginLeft: 10}}>
          <TouchableOpacity>
            <AntDesign name="bells" size={25} color={'white'} />
          </TouchableOpacity>
        </View>
      </View>

      {/* Name User */}
      <View style={{flex: 1}}>
        <Text style={styles.nameService}>{profile.name}</Text>

        {/* Menu */}
        <View style={styles.containerMenuServices}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <FlatList
              numColumns={3}
              data={servicesMenu}
              keyExtractor={item => item.idService}
              renderItem={({item}) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      dispatch(GetMenu(item.idService, item.servicesName));
                      navigation.navigate('services');
                    }}>
                    <View style={{alignItems: 'center'}}>
                      <Image style={styles.logoMenu} source={item.image} />
                      <Text style={styles.servicesName}>
                        {item.servicesName}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
          </View>
        </View>
        {/* Near You */}
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.nearYou}>Our Laundry</Text>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
            onPress={() => {
              dispatch(getNearYou());
              navigation.navigate('ListNearYou');
            }}>
            <Text style={styles.seeAll}>
              See All <AntDesign name="right" size={10} color={'black'} />
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{marginHorizontal: 10}}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={AllLaundry}
            renderItem={props => {
              return (
                <View style={{marginBottom: 10}}>
                  <CardNearYou
                    navigation={navigation}
                    laundryname={props.item.name}
                    rating={props.item.average_rating}
                    location={props.item.address.street}
                  />
                </View>
              );
            }}
          />
        </View>
        <CardNeedHelp />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    height: 50,
    marginTop: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  search: {
    backgroundColor: 'white',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 7,
    width: '90%',
    alignItems: 'flex-end',
  },
  nameService: {
    fontWeight: 'bold',
    color: 'white',
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 10,
    fontSize: 20,
  },
  logoMenu: {
    width: 100,
    height: null,
    aspectRatio: 1,
  },

  titleList: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  logo: {
    width: '100%',
    justifyContent: 'center',
    height: 266,
    position: 'absolute',
    flex: 1,
  },
  logoNotif: {
    width: '20%',
    flexDirection: 'row',
    height: 36,
    position: 'absolute',
    flex: 1,
  },
  nearYou: {
    fontWeight: 'bold',
    color: 'black',
    marginHorizontal: 20,
    marginTop: 30,
    marginBottom: 10,
    fontSize: 20,
  },
  seeAll: {
    color: 'black',
    marginHorizontal: 20,
    marginTop: 30,
    marginBottom: 10,
    fontSize: 13,
  },
  servicesName: {
    marginHorizontal: 15,
    fontSize: 13,
    color: '#4A696F',
    fontWeight: 'bold',
  },
  containerMenuServices: {
    backgroundColor: 'white',
    marginHorizontal: 20,
    borderRadius: 15,
  },
});

export default homePage;
