import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import MainMenuWashAndIron from '../assets/MainMenuWashAndIron.png';
import ProfileNumber from '../assets/ProfileNumber.png';
import Location from '../assets/Location.png';
import DistanceLocation from '../assets/DistanceLocation.png';
import AntDesign from 'react-native-vector-icons/AntDesign';
import ProcessLine from '../assets/ProcessLine.png';
import ProcessWash from '../assets/ProcessWash.png';
import ProcessDry from '../assets/ProcessDry.png';
import ProcessIron from '../assets/ProcessIron.png';
import ProcessFinish from '../assets/ProcessFinish.png';
import BareWash from '../assets/BareWash.png';
import BareDry from '../assets/BareDry.png';
import BareIron from '../assets/BareIron.png';
import BareFinish from '../assets/BareFinish.png';
import {ScrollView} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {GetOrderDetail} from '../Redux/action/ActionMenu';

const OrderDetailPage = props => {
  console.log('order detail page', props);

  const {orderDetail} = useSelector(state => state.AllLaundryReducers);
  console.log('orderDETAIl', orderDetail);
  const dispatch = useDispatch();

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <AntDesign
          name="left"
          size={20}
          color={'white'}
          onPress={() => props.navigation.goBack()}
        />
        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 16}}>
          Order Detail
        </Text>
        <Text></Text>
      </View>

      <ScrollView style={styles.container}>
        <View>
          <Text style={styles.BodyStatus}>Status</Text>
          <View style={styles.ContentStatus}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-around',
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  flexDirection: 'column',
                }}>
                <Text style={styles.ContenStatus_StatusName}>
                  {orderDetail?.orderstatus?.name}
                </Text>
                <Text style={{textAlign: 'left', marginBottom: 5}}>
                  Order ID {orderDetail.id}
                </Text>
              </View>
              <Text style={{color: '#7A7A7A', marginBottom: 5}}>
                Courier’s name : {orderDetail.courier}
              </Text>
              <View style={{flexDirection: 'row', marginHorizontal: 10}}>
                {/* washing */}
                {orderDetail.orderstatus?.name == 'Washing' ? (
                  <View style={styles.ActivePics}>
                    <Image
                      style={{
                        width: 15,
                        height: 15,
                      }}
                      source={BareWash}
                    />
                  </View>
                ) : (
                  <View>
                    <Image style={styles.NonActivePics} source={ProcessWash} />
                  </View>
                )}
                <Image style={styles.LinePics} source={ProcessLine} />

                {/* dry */}
                {orderDetail.orderstatus?.name == 'Drying' ? (
                  <View style={styles.ActivePics}>
                    <Image
                      style={{
                        width: 15,
                        height: 15,
                      }}
                      source={BareDry}
                    />
                  </View>
                ) : (
                  <View>
                    <Image style={styles.NonActivePics} source={ProcessDry} />
                  </View>
                )}
                <Image style={styles.LinePics} source={ProcessLine} />

                {/* Iron */}
                {orderDetail.orderstatus?.name == 'Ironing' ? (
                  <View style={styles.ActivePics}>
                    <Image
                      style={{
                        width: 13,
                        height: 13,
                      }}
                      source={BareIron}
                    />
                  </View>
                ) : (
                  <View>
                    <Image style={styles.NonActivePics} source={ProcessIron} />
                  </View>
                )}
                <Image style={styles.LinePics} source={ProcessLine} />

                {/* finish */}
                {orderDetail.orderstatus?.name == 'Finish' ? (
                  <View style={styles.ActivePics}>
                    <Image
                      style={{
                        width: 15,
                        height: 15,
                      }}
                      source={BareFinish}
                    />
                  </View>
                ) : (
                  <View>
                    <Image
                      style={styles.NonActivePics}
                      source={ProcessFinish}
                    />
                  </View>
                )}
              </View>
            </View>
          </View>

          {/* order laundry */}
          <View style={styles.BodyOrderLaundry}>
            <View>
              <Image
                style={{marginLeft: 10, width: 40, height: 40}}
                source={MainMenuWashAndIron}
              />
            </View>
            <View
              style={{
                flex: 1,
                marginLeft: 10,
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <View
                style={{
                  flexDirection: 'column',
                }}>
                <Text style={styles.BodyOrderLaundry_SevicesName}>
                  {orderDetail.service?.name} {orderDetail.service?.level?.name}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Text>{orderDetail.ordertype?.name} :</Text>
                  {orderDetail.item?.id == '' ? null : (
                    <View>
                      {orderDetail.item?.map(e => {
                        return (
                          <View style={{flexDirection: 'row', marginLeft: 10}}>
                            <Text>{e.name}</Text>
                            <Text> </Text>
                            <Text>{e.quantity}</Text>
                          </View>
                        );
                      })}
                    </View>
                  )}

                  {orderDetail.weight == 0 ? null : (
                    <View>
                      <Text>{orderDetail.weight}kg</Text>
                    </View>
                  )}
                </View>
              </View>
              <View>
                <Text style={{fontWeight: 'bold', color: '#2B495D'}}>
                  Rp {orderDetail.estimatePrice}
                </Text>
              </View>
            </View>
          </View>

          {/* laundry profile */}
          <View style={styles.BodyLaundryProfile}>
            <View
              style={{
                marginHorizontal: 10,
                flexDirection: 'column',
              }}>
              <Text style={styles.BodyLaundryProfile_NameLaundry}>
                {orderDetail.laundry?.name}
              </Text>
            </View>
            <View>
              <Image style={styles.phoneNumber} source={ProfileNumber} />
            </View>
          </View>

          {/* address */}
          <View style={styles.BodyAddress}>
            <Text style={styles.BodyAddress_delivery}>Delivery</Text>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={{
                    width: 40,
                    height: 40,
                    marginHorizontal: 10,
                  }}
                  source={Location}
                />
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'space-around',
                  marginHorizontal: 10,
                }}>
                <Text style={{color: '#7A7A7A', textAlign: 'left'}}>
                  Pick up at
                </Text>
                <Text>{orderDetail.pickUpAddress?.address}</Text>
                <Text>{orderDetail.pickUpAddress?.note}</Text>
              </View>
            </View>
            <View>
              <Image
                style={{
                  marginHorizontal: 30,
                  top: -20,
                  marginBottom: -25,
                }}
                source={DistanceLocation}
              />
            </View>
            <View
              style={{
                // backgroundColor: 'pink',
                flexDirection: 'row',
              }}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={{
                    width: 40,
                    height: 40,
                    marginHorizontal: 10,
                  }}
                  source={Location}
                />
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'space-around',
                  marginHorizontal: 10,
                }}>
                <Text style={{color: '#7A7A7A'}}>Deliver to</Text>
                <Text>{orderDetail.deliveryAddress?.address}</Text>
                <Text>{orderDetail.deliveryAddress?.note}</Text>
              </View>
            </View>
          </View>
          <View>
            <View>
              <Text
                style={{
                  marginVertical: 5,
                  marginHorizontal: 10,
                  fontWeight: 'bold',
                  fontSize: 18,
                }}>
                Payment Summary
              </Text>
            </View>
            <View
              style={{
                // backgroundColor: 'pink',
                padding: 10,
                marginHorizontal: 10,
                borderColor: '#EFEFEF',
                borderWidth: 1,
                borderRadius: 10,
                marginBottom: 10,
              }}>
              <View
                style={
                  {
                    // backgroundColor: 'yellow',
                  }
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={{marginRight: 5}}>Price</Text>
                  <Text style={{marginBottom: 5}}>
                    Rp {orderDetail.estimatePrice}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={{marginRight: 10}}>Delivery Fee</Text>
                  <Text style={{marginBottom: 10}}>
                    Rp {orderDetail.delivery_fee}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    borderBottomColor: 'gray',
                    borderBottomWidth: 1,
                  }}></View>
                <View
                  style={{
                    marginTop: 5,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={{marginRight: 10, fontWeight: 'bold'}}>
                    Total
                  </Text>
                  <Text style={{fontWeight: 'bold'}}>
                    Rp {orderDetail.totalPrice}
                  </Text>
                </View>
              </View>
            </View>

            {/* payment */}
            <View style={styles.BodyPayment}>
              <View
                style={{
                  flexDirection: 'column',
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#2B495D',
                  }}>
                  {orderDetail.paymentType}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default OrderDetailPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    backgroundColor: '#FF6C6C',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 20,
  },
  services: {
    borderColor: '#457899',
    borderWidth: 1,
    borderRadius: 20,
    marginRight: 10,
    marginBottom: 10,
  },
  types: {
    borderColor: '#457899',
    borderWidth: 1,
    borderRadius: 20,
    marginRight: 10,
    marginBottom: 10,
  },
  footerNext2: {
    width: '35%',
    backgroundColor: '#EFEFEF',
  },

  NextInputAddress2: {
    marginHorizontal: 10,
    textAlign: 'left',
    color: 'black',
  },
  ActivePics: {
    width: 33,
    height: 33,
    borderRadius: 100,
    backgroundColor: '#348CC5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  LinePics: {
    width: 50,
    top: 20,
  },
  NonActivePics: {
    width: 35,
    height: 35,
  },
  BodyStatus: {
    marginVertical: 10,
    marginHorizontal: 10,
    fontWeight: 'bold',
    fontSize: 18,
  },
  ContentStatus: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 5,
    marginHorizontal: 10,
    borderColor: '#EFEFEF',
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 10,
  },
  ContenStatus_StatusName: {
    textAlign: 'left',
    color: '#2B495D',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  BodyOrderLaundry: {
    flexDirection: 'row',
    padding: 10,
    borderColor: '#EFEFEF',
    borderWidth: 1,
    borderRadius: 10,
    marginHorizontal: 10,
    marginBottom: 10,
  },
  BodyOrderLaundry_SevicesName: {
    marginBottom: 10,
    color: '#2B495D',
    fontWeight: 'bold',
    marginBottom: 10,
  },
  BodyLaundryProfile: {
    marginHorizontal: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    // backgroundColor: 'yellow',
    padding: 10,
    borderColor: '#EFEFEF',
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 5,
  },
  BodyLaundryProfile_NameLaundry: {
    fontWeight: 'bold',
    color: '#2B495D',
    marginBottom: 5,
  },
  phoneNumber: {
    width: 15,
    justifyContent: 'space-between',
    height: 15,
    marginHorizontal: 10,
  },
  BodyAddress: {
    marginBottom: 10,
    marginHorizontal: 10,
    // backgroundColor: 'yellow',
  },
  BodyAddress_delivery: {
    marginHorizontal: 10,
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 10,
  },
  BodyPayment: {
    marginHorizontal: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 15,
    borderColor: '#EFEFEF',
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 10,
  },
});
