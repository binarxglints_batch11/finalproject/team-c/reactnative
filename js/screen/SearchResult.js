import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  FlatList,
  TouchableOpacity,
  TextInput,
  Text,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SearchBar from '../screen/Component/Search/index';
import BGHome from '../assets/BGHome.png';
import {useSelector} from 'react-redux';
import ImageLaundry from '../assets/ImageLaundry.png';
import ProfilePickupDriver from '../assets/ProfilePickupDriver.png';
import CardListNearYou from '../screen/Component/CardListNearYou/index';

export default function SearchResult(props) {
  const {search} = useSelector(state => state.AllLaundryReducers);

  const ServiceId = props.route.params.id;

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.logo} source={BGHome}>
        {/* header */}
        <View style={styles.header}>
          <SearchBar />
          <View style={{marginLeft: 10}}>
            <TouchableOpacity>
              <AntDesign name="bells" size={25} color={'white'} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{margin: 20}}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={ServiceId}
            keyExtractor={(item, index) => item.key}
            renderItem={({item}) => {
              console.log('ini item seacrh result', item);
              return (
                <View
                  style={{
                    height: 40,
                    flexDirection: 'row',
                    padding: 10,
                    backgroundColor: '#457899',
                    borderRadius: 20,
                    marginRight: 10,
                  }}>
                  <Text style={{color: 'white', paddingHorizontal: 10}}>
                    {item == 1
                      ? 'Wash and Iron'
                      : item == 2
                      ? 'Wash'
                      : item == 3
                      ? 'Iron'
                      : item == 4
                      ? 'Dry Clean'
                      : item == 5
                      ? 'Shoes'
                      : 'Household'}
                  </Text>
                </View>
              );
            }}
          />
        </View>
      </ImageBackground>

      <View style={styles.content}>
        {/* body */}
        <FlatList
          showsVerticalScrollIndicator={false}
          data={search}
          keyExtractor={item => item.id}
          renderItem={({item}) => {
            console.log('ini item', item);
            return (
              <View style={{flex: 1}}>
                <CardListNearYou
                  navigation={props.navigation}
                  id={item.id}
                  laundryname={item.name}
                  rating={item.average_rating}
                  location={item.address.street}
                  totalservices={item.total_services}
                  pickup={
                    item.pickUpAndDelivery == true
                      ? 'Pick Up & Delivery'
                      : 'Pick Up & Delivery not supported'
                  }
                />
              </View>
            );
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  search: {
    width: '80%',
    height: 36,
    marginTop: 7,
    // backgroundColor: 'white',
    color: 'black',
    borderRadius: 10,
  },
  header: {
    marginTop: 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  logo: {
    alignContent: 'center',
    height: 266,
  },
  logoNotif: {
    width: '20%',
    flexDirection: 'row',
    height: 36,
  },
  content: {
    flex: 1,
    marginTop: -150,
    paddingHorizontal: 20,
  },
});
