import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  TextInput,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useDispatch} from 'react-redux';
import {GetSeacrh} from '../Redux/action/ActionMenu';

const SearchPage = props => {
  const [checkButton, setcheckButton] = useState(false);
  const [servicesSelected, setServicesSelected] = useState([]);
  const [typesSelected, setTypesSelected] = useState([]);
  console.log(servicesSelected);
  console.log(typesSelected);

  const dispatch = useDispatch();

  const searchbyService = [
    {id: 1, services: 'Wash and Iron'},
    {id: 2, services: 'Wash'},
    {id: 3, services: 'Iron'},
    {id: 4, services: 'Dry Clean'},
    {id: 5, services: 'Shoes'},
    {id: 6, services: 'Household'},
  ];

  const searchbyTypes = [
    {id: 1, types: 'By Items'},
    {id: 2, types: 'By Kilogram'},
  ];

  const handlecheckButton = item => {
    setcheckButton(!checkButton);
  };

  const handleSelectService = id => {
    if (servicesSelected.includes(id)) {
      setServicesSelected(select => {
        return select.filter(servicesSelected => id !== servicesSelected);
      });
    } else {
      setServicesSelected([...servicesSelected, id]);
    }
  };

  const handleSelectTypes = id => {
    if (typesSelected.includes(id)) {
      setTypesSelected(selectType => {
        return selectType.filter(typesSelected => id !== typesSelected);
      });
    } else {
      setTypesSelected([...typesSelected, id]);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={styles.searchBar}>
          <AntDesign name="search1" size={25} color={'gray'} />
          <TextInput
            style={{marginLeft: 10, width: '80%'}}
            placeholder="Type laundry name"
          />
        </View>

        <TouchableOpacity
          style={{
            flex: 1,
            marginLeft: 10,
          }}
          onPress={async () => {
            await dispatch(GetSeacrh(servicesSelected, typesSelected));
            await props.navigation.navigate('SearchResult', {
              id: servicesSelected,
            });
          }}>
          <Text style={styles.textGo}>
            {servicesSelected.length > 0 || typesSelected.length > 0
              ? 'Go'
              : 'Cancel'}
          </Text>
        </TouchableOpacity>

        {/* Services */}
      </View>
      <View style={{padding: 20}}>
        <Text style={{marginTop: 20, marginBottom: 15}}>Select Services</Text>
        <FlatList
          numColumns={3}
          horizontal={false}
          data={searchbyService}
          keyExtractor={item => item.id}
          renderItem={({item}) => {
            return (
              <TouchableOpacity onPress={() => handleSelectService(item.id)}>
                <View
                  style={
                    servicesSelected.includes(item.id)
                      ? styles.servicesSelected
                      : styles.services
                  }>
                  <Text
                    style={
                      servicesSelected.includes(item.id)
                        ? styles.textServicesSelected
                        : styles.textServices
                    }>
                    {item.services}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>

      {/* Pick Up Delivey */}
      {/* <View style={{padding: 20}}>
        <Text>Select Delivery Service</Text>
      </View>

      <View
        style={{
          flexDirection: 'row',
          // marginTop: 10,
          marginBottom: 15,
          marginHorizontal: 20,
        }}>
        {checkButton === false ? (
          <TouchableOpacity
            style={styles.checkButton}
            onPress={handlecheckButton}>
            <MaterialIcons name="crop-square" size={30} color={'black'} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.checkButton}
            onPress={handlecheckButton}>
            <AntDesign
              name="checksquareo"
              size={30}
              color={'black'}
              style={{backgroundColor: 'white'}}
            />
          </TouchableOpacity>
        )}
        <Text
          style={{
            color: 'black',
            marginBottom: 10,
            marginTop: 7,
          }}>
          Pick Up Delivery
        </Text>
      </View> */}

      {/* types */}
      <View style={{padding: 20}}>
        <Text style={{marginTop: 20, marginBottom: 15}}>Select Types</Text>
        <FlatList
          numColumns={2}
          horizontal={false}
          data={searchbyTypes}
          keyExtractor={item => item.id}
          renderItem={({item}) => {
            return (
              <TouchableOpacity onPress={() => handleSelectTypes(item.id)}>
                <View
                  style={
                    typesSelected.includes(item.id)
                      ? styles.typesSelected
                      : styles.types
                  }>
                  <Text
                    style={
                      typesSelected.includes(item.id)
                        ? styles.textTypesSelected
                        : styles.textTypes
                    }>
                    {item.types}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </View>
  );
};

export default SearchPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    height: 50,
    marginTop: 10,
    marginHorizontal: 20,
    alignItems: 'center',
    flexDirection: 'row',
  },
  searchBar: {
    borderWidth: 1,
    borderColor: 'gray',
    flexDirection: 'row',
    padding: 5,
    alignItems: 'center',
    width: '80%',
  },
  services: {
    borderColor: '#457899',
    borderWidth: 1,
    borderRadius: 20,
    marginRight: 10,
    marginBottom: 10,
  },
  servicesSelected: {
    backgroundColor: '#457899',
    borderColor: '#457899',
    borderWidth: 1,
    borderRadius: 20,
    marginRight: 10,
    marginBottom: 10,
  },
  textServices: {
    paddingHorizontal: 17,
    paddingVertical: 7,
    color: '#457899',
  },
  textServicesSelected: {
    paddingHorizontal: 17,
    paddingVertical: 7,
    color: 'white',
  },
  types: {
    borderColor: '#457899',
    borderWidth: 1,
    borderRadius: 20,
    marginRight: 10,
    marginBottom: 10,
  },
  typesSelected: {
    backgroundColor: '#457899',
    borderColor: '#457899',
    borderWidth: 1,
    borderRadius: 20,
    marginRight: 10,
    marginBottom: 10,
  },
  textTypes: {
    paddingHorizontal: 17,
    paddingVertical: 7,
    color: '#457899',
  },
  textTypesSelected: {
    paddingHorizontal: 17,
    paddingVertical: 7,
    color: 'white',
  },
  textGo: {
    color: '#FF415B',
    fontSize: 14,
    fontWeight: '700',
    textAlign: 'center',
  },
});
